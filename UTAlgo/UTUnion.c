//
//  UTUnion.c
//  UTLib
//
//  Created by Maksym Kareta on 2/16/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

#include <stdio.h>
#include "UTUnion.h"

struct UTUnion
{
    size_t *root;
    size_t *treeSize;
    size_t size;
};

static size_t UTUnionRoot(UTUnionRef ref, size_t p);

UTUnionRef UTUnionNew(size_t size)
{
    UTUnionRef ref = malloc(sizeof(*ref));
    ref->root = malloc(sizeof(*ref->root) * size);
    ref->treeSize = malloc(sizeof(*ref->treeSize) * size);
    ref->size = size;
    UTUnionReset(ref);
    return ref;
}

void UTUnionReset(UTUnionRef ref)
{
    const size_t size = ref->size;
    for (size_t i = 0; i < size; i++)
    {
        ref->root[i] = i;
        ref->treeSize[i] = 1;
    }
}

void UTUnionFree(UTUnionRef ref)
{
    assert(ref != NULL);
    free(ref->root);
    free(ref->treeSize);
    free(ref);
}

size_t UTUnionRoot(UTUnionRef ref, size_t p)
{
    size_t temp = 0;
    while (p != ref->root[p])
    {
        temp = p;
        p = ref->root[p];
        ref->root[temp] = ref->root[p];
    }
    return p;
}

void UTUnionConnect(UTUnionRef ref, size_t p, size_t q)
{
    size_t root1 = UTUnionRoot(ref, p);
    size_t root2 = UTUnionRoot(ref, q);
    if (ref->treeSize[root1] >= ref->treeSize[root2])
    {
        ref->root[root2] = root1;
        ref->treeSize[root1] += ref->treeSize[root2];
    }
    else
    {
        ref->root[root1] = root2;
        ref->treeSize[root2] += ref->treeSize[root1];
    }
}

bool UTUnionIsConnected(UTUnionRef ref, size_t p, size_t q)
{
    return UTUnionRoot(ref, p) == UTUnionRoot(ref, q);
}


