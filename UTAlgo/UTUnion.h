//
//  UTUnion.h
//  UTLib
//
//  Created by Maksym Kareta on 2/16/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

#ifndef UTLib_UTUnion_h
#define UTLib_UTUnion_h

#include "UTDefinitionsC.h"

///Fast Union with balancing and compression
typedef struct UTUnion * UTUnionRef;

///Create and return new instance of UTUnion
UTUnionRef UTUnionNew(size_t size);

///Release UTUnion object
void UTUnionFree(UTUnionRef ref);

///Connect node p and node q
void UTUnionConnect(UTUnionRef ref, size_t p, size_t q);

///Return true if node p and q are connected
bool UTUnionIsConnected(UTUnionRef ref, size_t p, size_t q);

///Reset structure to initial state
void UTUnionReset(UTUnionRef ref);

#endif
