/*
 * File:    PZPointFunctions.h
 *
 * Created by Maksym Kareta on 1/23/12.
 * Copyright 2012 Maksym Kareta. All rights reserved.
 *
 */

#import <CoreGraphics/CGGeometry.h>

#ifdef __LP64__

static __inline__ CGFloat CGPointDistanceBetweenPoints(CGPoint p1, CGPoint p2)
{
    return sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y));
}

#else 

static __inline__ CGFloat CGPointDistanceBetweenPoints(CGPoint p1, CGPoint p2)
{
    return sqrtf((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y));
}

#endif

static __inline__ CGPoint CGPointMinusPoint(CGPoint p1, CGPoint p2)
{
	return CGPointMake(p1.x - p2.x, p1.y - p2.y);
}

static __inline__ CGPoint CGPointPlusPoint(CGPoint p1, CGPoint p2)
{
	return CGPointMake(p1.x + p2.x, p1.y + p2.y);
}
