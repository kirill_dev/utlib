/*
 * File:    UTVector.h
 *
 * Created by Maksym Kareta on 1/23/12.
 * Copyright 2012 Maksym Kareta. All rights reserved.
 *
 */

#import <CoreGraphics/CGGeometry.h>
#import "UTDefinitionsC.h"
#import "UTMath.h"

typedef CGPoint UTVector;

extern const UTVector UTVectorZero;

#ifdef UT_DEBUG //For type cheking in debug mode

static __inline__ CGPoint UTVectorConvertToPoint(UTVector x)
{
	return (CGPoint)x;
}

static __inline__ CGPoint UTVectorFromPoint(CGPoint x)
{
	return (UTVector)x;
}

#else

#define UTVectorConvertToPoint(x) ((CGPoint)x)
#define UTVectorFromPoint(x) ((UTVector)x)

#endif

UT_INLINE UTVector UTVectorMakeWithPoints(CGPoint x, CGPoint y);
UT_INLINE UTVector UTVectorVectorsAverage(UTVector vector1, UTVector vector2);
UT_STATIC_INLINE UTVector UTVectorMake(CGFloat x, CGFloat y)
{
    UTVector v;
    v.x = x;
    v.y = y;
    return v;
}

UT_INLINE UTVector UTVectorSumVectors(UTVector vector1, UTVector vector2);

UT_INLINE UTVector UTVectorSubtracVectors(UTVector minuend, UTVector subtrahend);
UT_INLINE UTVector UTVectorScale(UTVector vector, CGFloat sx, CGFloat sy);
UT_INLINE UTVector UTVectorMultipliedByScalar(UTVector vector, CGFloat scalar);

UT_STATIC_INLINE UTVector UTVectorRotate(UTVector vector, CGFloat angle)
{
    CGFloat c = UTCos(angle);
    CGFloat s = UTSin(angle);
    return UTVectorMake(c * vector.x - s * vector.y, s * vector.x + c * vector.y);
}

UT_INLINE UTVector UTVectorNormalize(UTVector vector);
UT_INLINE CGFloat UTVectorLength(UTVector vector);
UT_INLINE UTVector UTVectorSetLength(UTVector vector, CGFloat length);
UT_INLINE CGFloat UTVectorAngle(UTVector vector);
UT_INLINE CGFloat UTVectorAngleSin(UTVector vector);
UT_INLINE CGFloat UTVectorAngleCos(UTVector vector);
UT_INLINE CGFloat UTVectorWedgeProduct(UTVector vector1, UTVector vector2);
UT_INLINE CGFloat UTVectorAngleBetweenVectors(UTVector vector1, UTVector vector2);
UT_INLINE CGFloat UTVectorSinOfAngleBetweenVectors(UTVector vector1, UTVector vector2);
UT_INLINE CGFloat UTVectorScalarMul(UTVector vec1, UTVector vec2);
/*
 * Result is vector1 + step * vector2
 * See impl for details
 */
UT_INLINE UTVector UTVectorSmoothStep(UTVector vector1, UTVector vector2, CGFloat step);


UT_INLINE void UTVectorMultiplyByScalar(UTVector *inOutVector1, CGFloat scalar);
UT_INLINE void UTVectorAddVector(UTVector *inOutVector1, UTVector vector2);

