//
//  UTVector.m
//  UsefulThings
//
//  Created by Maksym Kareta on 3/8/12.
//  Copyright (c) 2012 Maksym Kareta. All rights reserved.
//

#import "UTVector.h"

const UTVector UTVectorZero = {0, 0};

UTVector UTVectorMakeWithPoints(CGPoint x, CGPoint y)
{
	return UTVectorSubtracVectors(x, y);
}

UTVector UTVectorVectorsAverage(UTVector vector1, UTVector vector2)
{
	return UTVectorMake((vector1.x + vector2.x) / 2.0f, (vector1.y + vector2.y) / 2.0f);
}

UTVector UTVectorSumVectors(UTVector vector1, UTVector vector2)
{
	return UTVectorMake(vector1.x + vector2.x, vector1.y + vector2.y);
}

void UTVectorAddVector(UTVector *inOutVector1, UTVector vector2)
{
	inOutVector1->x += vector2.x;
	inOutVector1->y += vector2.y;
}

UTVector UTVectorSubtracVectors(UTVector minuend, UTVector subtrahend)
{
	return UTVectorMake(minuend.x - subtrahend.x, minuend.y - subtrahend.y);
}

UTVector UTVectorScale(UTVector vector, CGFloat sx, CGFloat sy)
{
	return UTVectorMake(vector.x * sx, vector.y * sy);
}

UTVector UTVectorMultipliedByScalar(UTVector vector, CGFloat scalar)
{
	return UTVectorMake(vector.x * scalar, vector.y * scalar);
}


UTVector UTVectorNormalize(UTVector vector)
{
	CGFloat length = UTSqrt(vector.x * vector.x + vector.y * vector.y);
	return UTVectorMake(vector.x / length, vector.y / length);
}

CGFloat UTVectorLength(UTVector vector)
{
	return UTSqrt(vector.x * vector.x + vector.y * vector.y);
}

UTVector UTVectorSetLength(UTVector vector, CGFloat length)
{
	CGFloat oldLength = UTSqrt(vector.x * vector.x + vector.y * vector.y);
	return oldLength > 0 ? UTVectorMake((vector.x / oldLength) * length, (vector.y / oldLength) * length) : UTVectorZero;
}

UTVector UTVectorSmoothStep(UTVector vector1, UTVector vector2, CGFloat step)
{
	return UTVectorMake((vector2.x - vector1.x) * step + vector1.x, (vector2.y - vector1.y) * step + vector1.y);
	/* Equal to this code */
	/*
	 UTVector stepVector = UTVectorSubtracVectors(vector2, vector1);
	 stepVector = UTVectorMultiplyByScalar(stepVector, step);
	 stepVector = UTVectorSumVectors(stepVector, vector1);
	 return stepVector;
	 */
}

CGFloat UTVectorScalarMul(UTVector vec1, UTVector vec2)
{
//	<X, Y> = x1 * y1 + x2 * y2;
	return vec1.x * vec2.x + vec1.y * vec2.y; 
}

CGFloat UTVectorAngle(UTVector vector)
{
	CGFloat dist = UTVectorLength(vector);
	return UTAcos(vector.x / dist);
}

CGFloat UTVectorAngleSin(UTVector vector)
{
	CGFloat dist = UTVectorLength(vector);
	return (vector.y / dist);
}

CGFloat UTVectorAngleCos(UTVector vector)
{
	CGFloat dist = UTVectorLength(vector);
	return (vector.x / dist);
}

CGFloat UTVectorWedgeProduct(UTVector vector1, UTVector vector2)
{
	return vector1.x * vector2.y - vector2.x * vector1.y;
}

CGFloat UTVectorAngleBetweenVectors(UTVector vector1, UTVector vector2)
{
	return UTAsin(UTVectorSinOfAngleBetweenVectors(vector1, vector2));
}

CGFloat UTVectorSinOfAngleBetweenVectors(UTVector vector1, UTVector vector2)
{
	CGFloat wedge = UTVectorWedgeProduct(vector1, vector2);
	CGFloat l1 = UTVectorLength(vector1);
	CGFloat l2 = UTVectorLength(vector2);
	return wedge / (l1 * l2);
}

void UTVectorMultiplyByScalar(UTVector *inOutVector1, CGFloat scalar)
{
    inOutVector1->x *= scalar;
	inOutVector1->y *= scalar;
}
