//
//  UTMath.h
//  UTLib
//
//  Created by Maksym Kareta on 3/16/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

#import <Foundation/Foundation.h>

extern float __attribute__((overloadable)) UTCos(const float val);
extern double __attribute__((overloadable)) UTCos(const double val);
extern float __attribute__((overloadable)) UTAcos(const float val);
extern double __attribute__((overloadable)) UTAcos(const double val);
extern float __attribute__((overloadable)) UTSin(const float val);
extern double __attribute__((overloadable)) UTSin(const double val);
extern float __attribute__((overloadable)) UTAsin(const float val);
extern double __attribute__((overloadable)) UTAsin(const double val);
extern float __attribute__((overloadable)) UTSqrt(const float val);
extern double __attribute__((overloadable)) UTSqrt(const double val);

