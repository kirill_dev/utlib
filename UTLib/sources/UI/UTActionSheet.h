//
//  UTActionSheet.h
//  UTLib
//
//  Created by Maksym Kareta on 12/17/13.
//  Copyright (c) 2013 Maksym Kareta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GCD+UTFunctions.h"

///Implements action sheet with block support
///Should be created only through new/init. You should readd all buttons before reuse.
@interface UTActionSheet : UIActionSheet

@property (weak, nonatomic) id <UIActionSheetDelegate> actionSheetDelegate;

///Adds a custom button to the action sheet.
///@param title The title of the new button.
///@param block The Block that will be called when user tap on the button.
///@return The index of the new button. Button indices start at 0 and increase in the order they are added.
- (NSInteger)addButtonWithTitle:(NSString *)title block:(UTEmptyBlock)block;

@end
