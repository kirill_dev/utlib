//
//  UTScrollViewController.h
//  UTLib
//
//  Created by Maksym Kareta on 5/6/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UTScrollViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIScrollView* scrollView;

- (void)resignFirstResponderFromCurrentResponder;

@end
