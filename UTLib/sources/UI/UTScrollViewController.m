//
//  UTScrollViewController.m
//  UTLib
//
//  Created by Maksym Kareta on 5/6/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

#import "UTScrollViewController.h"
#import "UIView+UTMethods.h"

static const CGFloat kDefaultKeyboardOffsetY = 8.f;

@interface UTScrollViewController ()

@end

@implementation UTScrollViewController

- (void)registerKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)unregisterKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self registerKeyboardNotifications];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self resignFirstResponderFromCurrentResponder];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super viewWillDisappear:animated];
}

#pragma mark -

- (void)resignFirstResponderFromCurrentResponder
{
    [self.view endEditing:YES];
}

#pragma mark - keyboard notifications
- (void)keyboardWillShow:(NSNotification *)notification
{
    NSDictionary *userInfo = [notification userInfo];
    CGRect rect = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    rect = [self.view convertRect:rect fromView:nil];
    NSTimeInterval animationDuration = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    UIEdgeInsets inset = UIEdgeInsetsMake(0, 0, rect.size.height, 0);
    
    [UIView animateWithDuration:animationDuration animations:^{
        [self.scrollView setContentInset:inset];
        [self.scrollView setScrollIndicatorInsets:inset];
    }];
    
    UIResponder *firstResponder = [self.view firstResponder];
    CGRect firstResponderFrame = CGRectZero;
    if ([firstResponder isKindOfClass:[UIView class]])
    {
        UIView *firstResponderView = (UIView *)firstResponder;
        firstResponderFrame = [self.scrollView convertRect:firstResponderView.frame fromView:[firstResponderView superview]];
        firstResponderFrame.origin.y -= kDefaultKeyboardOffsetY;
        firstResponderFrame.size.height += 2 * kDefaultKeyboardOffsetY;
        [self.scrollView scrollRectToVisible:firstResponderFrame animated:YES];
    }
    [self.scrollView flashScrollIndicators];
    
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    NSTimeInterval animationDuration = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:animationDuration animations:^{
        self.scrollView.contentInset = UIEdgeInsetsZero;
        self.scrollView.scrollIndicatorInsets = UIEdgeInsetsZero;
    }];
}

@end
