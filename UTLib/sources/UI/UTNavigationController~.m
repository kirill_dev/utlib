//
//  SKConcretSceneDrawer.m
//  SkiMan
//
//  Created by Oleg Zinko on 4/3/12.
//  Copyright (c) 2012 Academy. All rights reserved.
//

#import "UTNavigationController.h"
#import "UIView+animations.h"
#import "NSArray+UTMethods.h"

static const CGRect landscapeFrame = { {0, 0}, {480, 320} };
static const CGRect portraitFrame = { {0, 0}, {320, 480} };

static const CGFloat kSKConcretSceneDrawerDefaultAnimationTime = 0.4f;

@interface UTNavigationController()

@property (strong, nonatomic) NSMutableArray *controllerStack;
@property (strong, nonatomic) UIColor *backgroundColor;

@property (assign, nonatomic) CGFloat currentAnimationTime;
@property (strong, nonatomic) UTNavigationControllerItem *currentItem;

@property (assign, nonatomic) SKConcretSceneDrawerAppearingDirection direction;
@property (assign, nonatomic) CGRect screenRect;
@property (assign, nonatomic) CGRect topScreenRect;
@property (assign, nonatomic) CGRect bottomScreenRect;
@property (assign, nonatomic) CGRect leftScreenRect;
@property (assign, nonatomic) CGRect rightScreenRect;
@property (assign, nonatomic) CGRect startPosition;
@property (assign, nonatomic) CGRect finishPosition;

- (void)setUpScreenRects;
- (void)fadeInFadeOutWithColor:(UIColor *)color viewToFade:(UIView *)viewToFade viewToShow:(UIView *)viewToShow animationTime:(CGFloat)animationTime;
- (void)chooseStartPostitionfromDirection:(SKConcretSceneDrawerAppearingDirection)direction invertion:(BOOL)invertion;
- (void)animateWithInvertion:(BOOL)invertion;

@end

@implementation UTNavigationController

- (id)initWithRootViewController:(UIViewController *)controller
{
	self = [super init];
	if (nil != self) 
	{
		self.controllerStack = [NSMutableArray array];
		UTNavigationControllerItem *newItem = [UTNavigationControllerItem new];
		newItem.controller = controller;
		newItem.direction = 0;
		[self.controllerStack addObject:newItem];
		[self.view addSubview:controller.view];
		[self setUpScreenRects];
		self.animationTime = kSKConcretSceneDrawerDefaultAnimationTime;
	}
	return self;
}

- (BOOL)isFadingAnimation
{
	return (kSKConcretSceneDrawerAppearingFadeBlack == self.direction);
}

- (void)fadeInFadeOutWithColor:(UIColor *)color viewToFade:(UIView *)viewToFade viewToShow:(UIView *)viewToShow animationTime:(CGFloat)animationTime
{
	UIView *topView = [[UIView alloc] init];
	[topView setFrame:self.screenRect];
	[topView setBackgroundColor:color];
	[topView setAlpha:0.0f];
	
	[viewToFade setFrame:self.screenRect];
	[viewToFade setAlpha:1.0f];
	
	[viewToShow setFrame:self.screenRect];
	[viewToShow setAlpha:1.0f];
	
	[self.view addSubview:topView];

	[self.view prepareForAnimationSequence];
	
	[self.view addAnimationBlock:^{
								[topView setAlpha:1.0f];
							} completion:^{
								[viewToFade removeFromSuperview];
								[self.view insertSubview:viewToShow belowSubview:topView];
							} duration:animationTime / 2.0];
	[self.view addAnimationBlock:^{
								[topView setAlpha:0.0f];
							} completion:^{
								[topView removeFromSuperview];
							} duration:animationTime / 2.0];
	
	[self.view commitAnimationSequenceWithCompletionBlock:NULL];
}

- (void)chooseStartPostitionfromDirection:(SKConcretSceneDrawerAppearingDirection)direction invertion:(BOOL)invertion
{
	if (kSKConcretSceneDrawerAppearingDirectionDown == direction)
	{
		self.startPosition = self.topScreenRect;
		self.finishPosition = self.bottomScreenRect;
	} 
	if (kSKConcretSceneDrawerAppearingDirectionUp == direction)
	{
		self.startPosition = self.bottomScreenRect;
		self.finishPosition = self.topScreenRect;
	}	
	if (kSKConcretSceneDrawerAppearingDirectionRight == direction)
	{
		self.startPosition = self.leftScreenRect;
		self.finishPosition = self.rightScreenRect;
	}
	if (kSKConcretSceneDrawerAppearingDirectionLeft == direction)
	{
		self.startPosition = self.rightScreenRect;
		self.finishPosition = self.leftScreenRect;
	}
	
	if (kSKConcretSceneDrawerAppearingFadeBlack == direction)
	{
		self.startPosition = self.screenRect;
		self.finishPosition = self.screenRect;
		self.backgroundColor = [UIColor blackColor];
	}
	
	if (invertion)
	{
		CGRect temp = self.startPosition;
		self.startPosition = self.finishPosition;
		self.finishPosition = temp;
	}
}

- (void)animateWithInvertion:(BOOL)invertion
{
	[self chooseStartPostitionfromDirection:self.direction invertion:invertion];
	
	UTNavigationControllerItem *item = [self.controllerStack lastObject];
	
	if ([self isFadingAnimation])
	{
		[self fadeInFadeOutWithColor:self.backgroundColor viewToFade:self.currentItem.controller.view viewToShow:item.controller.view animationTime:self.currentAnimationTime];
	}
	else
	{
		[item.controller.view setFrame:self.startPosition];
		[self.view addSubview:item.controller.view];
		[UIView animateWithDuration:self.currentAnimationTime
						 animations:^{
							 [item.controller.view setFrame:self.screenRect];
							 [self.currentItem.controller.view setFrame:self.finishPosition];
						 }
						 completion:^(BOOL finished){
							 [self.currentItem.controller.view removeFromSuperview];
						 }];
	}
}

- (void)pushController:(UIViewController *)controller direction:(SKConcretSceneDrawerAppearingDirection)direction animationTime:(CGFloat)animationTime
{
	[self setUpScreenRects];
	self.currentItem = [self.controllerStack lastObject];
	UTNavigationControllerItem *item = [UTNavigationControllerItem new];
	item.controller = controller;
	item.direction = direction;
	item.animationTime = animationTime;
	[self.controllerStack addObject:item];
	
	[self prepareForAnimationWithItem:item];
	
	if (kSKConcretSceneDrawerAppearingDirectionNone == direction)
	{
		[item.controller.view setFrame:self.screenRect];
		[self.view addSubview:item.controller.view];
		[self.currentItem.controller.view removeFromSuperview];
		return;
	}
	
	[self animateWithInvertion:NO];
}

- (void)pushController:(UIViewController *)controller direction:(SKConcretSceneDrawerAppearingDirection)direction
{
	[self pushController:controller direction:direction animationTime:self.animationTime];
}

- (void)popControllerAnimated:(BOOL)animated
{
	[self setUpScreenRects];
	self.currentItem = [self.controllerStack lastObject];
	[self.controllerStack removeLastObject];
	UTNavigationControllerItem *item = [self.controllerStack lastObject];
	
	[self prepareForAnimationWithItem:self.currentItem];
	
	if (kSKConcretSceneDrawerAppearingDirectionNone == self.direction || !animated)
	{
		[item.controller.view setFrame:self.screenRect];
		[self.view addSubview:item.controller.view];
		[self.currentItem.controller.view removeFromSuperview];
		return;
	}
	
	[self animateWithInvertion:YES];
}

- (void)popToViewController:(UIViewController *)viewController
{
	//TODO: probably we can skip all view controllers with out popControllerAnimated: ans speed up this code
	while (self.controllerStack.lastObject != viewController && self.controllerStack.count > 1)
	{
		[self popControllerAnimated:NO];
	}
}

- (void)popToRootViewController
{
	[self popToViewController:self.controllerStack.firstObject];
}

- (void)setUpScreenRects
{
	if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation))
	{
		_screenRect = landscapeFrame;
	}
	else
	{
		_screenRect = portraitFrame;
	}
	_rightScreenRect = _screenRect;
	_rightScreenRect.origin.x = _screenRect.size.width;
	_leftScreenRect = _screenRect;
	_leftScreenRect.origin.x = -_screenRect.size.width;
	_topScreenRect = _screenRect;
	_topScreenRect.origin.y = -_screenRect.size.height;
	_bottomScreenRect = _screenRect;
	_bottomScreenRect.origin.y = _screenRect.size.height;
}

- (void)prepareForAnimationWithItem:(UTNavigationControllerItem *)item
{
	self.startPosition = self.screenRect;
	self.finishPosition = self.screenRect;
	self.backgroundColor = [UIColor blackColor];
	self.currentAnimationTime = [item animationTime];
	self.direction = [item direction];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
	return YES;
}

@end

@implementation UTNavigationControllerItem @end