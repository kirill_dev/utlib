//
//  UTAlertView.h
//  UTLib
//
//  Created by Kames on 9/1/13.
//  Copyright (c) 2013 Maksym Kareta. All rights reserved.
//

#import "GCD+UTFunctions.h"

/// @discussion This class encapsulate UIAlertView and provide methods that allows use blocks as button action.
/// Object designed to be used in simple scenarios and don't provide any way to customzize alert view behaviour.
@interface UTAlertView : NSObject

/// This method create UTAlertView
///
/// @param title Alert title
/// @param message Alert message
/// @param okBlock Block that will be called when user tap OK button
/// @param cancelBlock Block that will be called when user tap Cancel button
/// @param cancelButtonTitle Cancel button title
/// @param okButtonTitle OK button title
/// @return New UTAlertView object
- (instancetype)initWithTitle:(NSString *)title message:(NSString *)message okButtonTitle:(NSString *)okButtonTitle cancelButtonTitle:(NSString *)cancelButtonTitle okButtonBlock:(UTEmptyBlock)okBlock cancelBlock:(UTEmptyBlock)cancelBlock;

/// Create and return new UTAlertView instance
///
/// @param title Alert title
/// @param message Alert message
/// @param okBlock Block that will be called when user tap OK button
/// @param cancelBlock Block that will be called when user tap Cancel button
/// @param cancelButtonTitle Cancel button title
/// @param okButtonTitle OK button title
/// @return New UTAlertView object
+ (instancetype)alertViewWithTitle:(NSString *)title message:(NSString *)message okButtonTitle:(NSString *)okButtonTitle cancelButtonTitle:(NSString *)cancelButtonTitle okButtonBlock:(UTEmptyBlock)okBlock cancelBlock:(UTEmptyBlock)cancelBlock;

/// This method forward show action to UIAlertView
- (void)show;

@end
