//
//  UTActionSheet.m
//  UTLib
//
//  Created by Maksym Kareta on 12/17/13.
//  Copyright (c) 2013 Maksym Kareta. All rights reserved.
//

#import "UTActionSheet.h"
#import "GCD+UTFunctions.h"

@interface UTActionSheetAction : NSObject

@property (assign, nonatomic) NSInteger index;
@property (copy, nonatomic) UTEmptyBlock block;

@end


@interface UTActionSheet() <UIActionSheetDelegate>

@property (strong, nonatomic) NSMutableArray *actionButtons;

@end


@implementation UTActionSheet

- (id)init
{
    self = [super init];
    if (nil != self)
    {
        self.actionButtons = [NSMutableArray array];
        [super setDelegate:self];
    }
    return self;
}

- (NSInteger)addButtonWithTitle:(NSString *)title block:(UTEmptyBlock)block
{
    UTActionSheetAction *newAction = [UTActionSheetAction new];
    newAction.block = block;
    newAction.index = [self addButtonWithTitle:title];
    [self.actionButtons addObject:newAction];
    return newAction.index;
}

#pragma mark - redirect methods

- (void)setDelegate:(id <UIActionSheetDelegate>)delegate
{
    NSAssert(nil == delegate || delegate == self, @"");
    [super setDelegate:delegate];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    for (UTActionSheetAction *action in self.actionButtons)
    {
        if (action.index == buttonIndex)
        {
            UTInvokeBlock(action.block);
        }
    }
    id <UIActionSheetDelegate> delegate = self.actionSheetDelegate;
    [delegate actionSheet:actionSheet clickedButtonAtIndex:buttonIndex];
}

- (void)actionSheetCancel:(UIActionSheet *)actionSheet
{
    id <UIActionSheetDelegate> delegate = self.actionSheetDelegate;
    [delegate actionSheetCancel:actionSheet];
}

- (void)willPresentActionSheet:(UIActionSheet *)actionSheet
{
    id <UIActionSheetDelegate> delegate = self.actionSheetDelegate;
    [delegate willPresentActionSheet:actionSheet];
}

- (void)didPresentActionSheet:(UIActionSheet *)actionSheet
{
    id <UIActionSheetDelegate> delegate = self.actionSheetDelegate;
    [delegate didPresentActionSheet:actionSheet];
}

- (void)actionSheet:(UIActionSheet *)actionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    id <UIActionSheetDelegate> delegate = self.actionSheetDelegate;
    [delegate actionSheet:actionSheet willDismissWithButtonIndex:buttonIndex];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    id <UIActionSheetDelegate> delegate = self.actionSheetDelegate;
    [delegate actionSheet:actionSheet didDismissWithButtonIndex:buttonIndex];
    [self.actionButtons removeAllObjects];
}

@end

@implementation UTActionSheetAction @end
