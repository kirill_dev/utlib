//
//  UTAnimationSequence.h
//  UTLib
//
//  Created by Maksym Kareta on 3/4/15.
//  Copyright (c) 2015 Maksym Kareta. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^UIViewUTAnimationsSequenceBlock)(void);
typedef void (^UIViewUTAnimationsSequenceCompletionBlock)(void);

@interface UTAnimationSequence : NSObject

- (void)addAnimationWithDuration:(NSTimeInterval)duration block:(UIViewUTAnimationsSequenceBlock)block;
- (void)addAnimationWithDuration:(NSTimeInterval)duration block:(UIViewUTAnimationsSequenceBlock)block completion:(UIViewUTAnimationsSequenceCompletionBlock)completion;
- (void)commitAnimationSequenceWithCompletionBlock:(UIViewUTAnimationsSequenceCompletionBlock)block;
- (void)commitAnimationSequence;

@end
