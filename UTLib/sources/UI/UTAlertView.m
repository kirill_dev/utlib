//
//  UTAlertView.m
//  UTLib
//
//  Created by Kames on 9/1/13.
//  Copyright (c) 2013 Maksym Kareta. All rights reserved.
//

#import "UTAlertView.h"

@interface UTAlertView()

@property (strong, nonatomic) UIAlertView *view;
@property (copy, nonatomic) UTEmptyBlock okBlock;
@property (copy, nonatomic) UTEmptyBlock cancelBlock;
@property (weak, nonatomic) id internalDelegate;
@property (strong, nonatomic) id selfRetain;

@end

@implementation UTAlertView

- (instancetype)initWithTitle:(NSString *)title message:(NSString *)message okButtonTitle:(NSString *)okButtonTitle cancelButtonTitle:(NSString *)cancelButtonTitle okButtonBlock:(UTEmptyBlock)okBlock cancelBlock:(UTEmptyBlock)cancelBlock
{
    self = [super init];
    if (nil != self)
    {
        self.view = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:cancelButtonTitle otherButtonTitles:okButtonTitle, nil];
        self.okBlock = okBlock;
        self.cancelBlock = cancelBlock;
    }
    return self;
}

+ (instancetype)alertViewWithTitle:(NSString *)title message:(NSString *)message okButtonTitle:(NSString *)okButtonTitle cancelButtonTitle:(NSString *)cancelButtonTitle okButtonBlock:(UTEmptyBlock)okBlock cancelBlock:(UTEmptyBlock)cancelBlock
{
    return [[self alloc] initWithTitle:title message:message okButtonTitle:okButtonTitle cancelButtonTitle:cancelButtonTitle okButtonBlock:okBlock cancelBlock:cancelBlock];
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == alertView.cancelButtonIndex)
    {
        if (NULL != self.cancelBlock)
        {
            self.cancelBlock();
        }
    }
    else
    {
        if (NULL != self.okBlock)
        {
            self.okBlock();
        }
    }
    self.cancelBlock = NULL;
    self.okBlock = NULL;
    self.selfRetain = nil;
}

- (void)show
{
    [self.view show];
    self.selfRetain = self;
}

@end
