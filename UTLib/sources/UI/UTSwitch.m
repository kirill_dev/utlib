//
//  UTSwitch.m
//  UTLib
//
//  Created by Maksym Kareta on 12/2/13.
//  Copyright (c) 2013 Maksym Kareta. All rights reserved.
//

#import "UTSwitch.h"
#import "GCD+UTFunctions.h"
#import "UIColor+UTMethods.h"
#import "UIView+UTMethods.h"

#import <QuartzCore/QuartzCore.h>

static const CGFloat kDefaultThumbInset = 3.0f;
static const CGFloat kAnimateDuration = 0.3f;
static const CGFloat kRectShapeCornerRadius = 4.0f;
static const CGFloat kThumbShadowOpacity = 0.3f;
static const CGFloat kThumbShadowRadius = 0.5f;
static const CGFloat kSwitchBorderWidth = 1.75f;

@interface UTSwitch ()

@property (strong, nonatomic) UIImageView *onBackgroundView;
@property (strong, nonatomic) UIImageView *offBackgroundView;
@property (strong, nonatomic) UIImageView *backgroundView;
@property (strong, nonatomic) UIImageView *thumbView;

@end

@implementation UTSwitch

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (nil != self)
    {
        [self setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setup];
}

- (void)setup
{
    self.thumbInset = kDefaultThumbInset;
    [self setBackgroundColor:[UIColor clearColor]];

    self.backgroundView = [[UIImageView alloc] initWithFrame:self.fullFrame];
    [self.backgroundView setBackgroundColor:nil];
    [self addSubview:self.backgroundView];
    
    self.onBackgroundView = [[UIImageView alloc] initWithFrame:self.fullFrame];
    [self.onBackgroundView setBackgroundColor:UTRGBColor(19, 121, 208)];
    [self addSubview:self.onBackgroundView];
    
    self.offBackgroundView = [[UIImageView alloc] initWithFrame:self.fullFrame];
    [self.offBackgroundView setBackgroundColor:[UIColor whiteColor]];
    [self addSubview:self.offBackgroundView];
    
    self.thumbView = [[UIImageView alloc] initWithFrame:CGRectMake(self.thumbInset, self.thumbInset, self.frameHeight - 2 * self.thumbInset, self.frameHeight - 2 * self.thumbInset)];
    [self.thumbView setBackgroundColor:[UIColor whiteColor]];
    [self.thumbView.layer setCornerRadius:(self.frame.size.height - self.thumbInset) / 2];
    [self.thumbView setUserInteractionEnabled:YES];
    
    [self addSubview:self.thumbView];
    self.shadow = YES;
    
    self.type = kUTSwitchTypeRectangle;

    [self addDefaultGestureRecognizers];
    [self setOn:NO];
}

- (void)setThumbInset:(CGFloat)thumbInset
{
    _thumbInset = thumbInset;
    CGPoint center = self.thumbView.center;
    self.thumbView.frame = CGRectMake(self.thumbInset, self.thumbInset, self.frameHeight - 2 * self.thumbInset, self.frameHeight - 2 * self.thumbInset);
    self.thumbView.center = center;
    [self setType:self.type];
}

- (void)addDefaultGestureRecognizers
{
    UITapGestureRecognizer *tap = [UITapGestureRecognizer new];
    UITapGestureRecognizer *tapBackground = [UITapGestureRecognizer new];
    UIPanGestureRecognizer *pan = [UIPanGestureRecognizer new];
    UISwipeGestureRecognizer *leftSwipe = [UISwipeGestureRecognizer new];
    leftSwipe.direction = UISwipeGestureRecognizerDirectionLeft;
    UISwipeGestureRecognizer *rightSwipe = [UISwipeGestureRecognizer new];

    [pan setMaximumNumberOfTouches:1];
    [pan setMinimumNumberOfTouches:1];
    NSArray *ar = @[tap, tapBackground, pan, leftSwipe, rightSwipe];
    for (UIGestureRecognizer *gr in ar)
    {
        [gr setCancelsTouchesInView:NO];
        [gr setDelaysTouchesBegan:NO];
        [gr setDelaysTouchesEnded:NO];
    }

    [tap requireGestureRecognizerToFail:pan];
    
    [leftSwipe addTarget:self action:@selector(swipe:)];
    [rightSwipe addTarget:self action:@selector(swipe:)];
    [tap addTarget:self action:@selector(tap:)];
    [tapBackground addTarget:self action:@selector(tap:)];
    [pan addTarget:self action:@selector(pan:)];
    [pan setDelegate:self];
    
    [self addGestureRecognizer:rightSwipe];
    [self addGestureRecognizer:leftSwipe];
    [self addGestureRecognizer:tapBackground];
    [self.thumbView addGestureRecognizer:tap];
    [self.thumbView addGestureRecognizer:pan];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}


#pragma mark - Accessor

- (void)setOn:(BOOL)on animated:(BOOL)animated
{
    if (animated)
    {
        [UIView animateWithDuration:kAnimateDuration animations:^{
            [self setOn:on];
        }];
    }
    else
    {
        [self setOn:on];
    }
}

- (void)setOn:(BOOL)on
{
    _on = on;
    
    CGFloat onAlpha = 1.0;
    CGAffineTransform offTranform = CGAffineTransformIdentity;
    CGFloat x = 0;
    if (on)
    {
        onAlpha = 1.0;
        offTranform = CGAffineTransformMakeScale(0.0, 0.0);
        x = self.onBackgroundView.frame.size.width - (self.thumbView.frameWidth + self.thumbInset) / 2.0f - self.onStateInset;
    }
    else
    {
        onAlpha = 0.0;
        offTranform = CGAffineTransformMakeScale(1.0, 1.0);
        x = (self.thumbView.frameWidth + self.thumbInset) / 2.0f + self.offStateInset;
    }
    
    self.thumbView.center = CGPointMake(x, self.frameHeight / 2.0f);
    
    if (self.shouldAnimateTransform)
    {
        [self.onBackgroundView setAlpha:onAlpha];
        self.offBackgroundView.transform = offTranform;
    }
    else
    {
        [UIView performWithoutAnimation:^{
            [self.onBackgroundView setAlpha:onAlpha];
            self.offBackgroundView.transform = offTranform;
        }];
    }
}

- (void)setType:(UTSwitchType)type
{
    _type = type;
    switch (type)
    {
        case kUTSwitchTypeOval:
            [self.onBackgroundView.layer setCornerRadius:self.frame.size.height / 2.0f];
            [self.offBackgroundView.layer setCornerRadius:self.frame.size.height / 2.0f];
            [self.thumbView.layer setCornerRadius:self.thumbView.frameHeight / 2.0f];
            break;
        case kUTSwitchTypeRectangle:
            [self.onBackgroundView.layer setCornerRadius:kRectShapeCornerRadius];
            [self.offBackgroundView.layer setCornerRadius:kRectShapeCornerRadius];
            [self.thumbView.layer setCornerRadius:kRectShapeCornerRadius];
        default:
            [self.onBackgroundView.layer setCornerRadius:0];
            [self.offBackgroundView.layer setCornerRadius:0];
            [self.thumbView.layer setCornerRadius:0];
            break;
    }
}

- (void)setShadow:(BOOL)shadow
{
    _shadow = shadow;
    
    if (shadow)
    {
        [self.thumbView.layer setShadowOffset:CGSizeMake(0, 1)];
        [self.thumbView.layer setShadowRadius:kThumbShadowRadius];
        [self.thumbView.layer setShadowOpacity:kThumbShadowOpacity];
    }
    else
    {
        [self.thumbView.layer setShadowRadius:0.0];
        [self.thumbView.layer setShadowOpacity:0.0];
    }
}

#pragma mark - Gesture Recognizers

- (void)pan:(UIPanGestureRecognizer *)recognizer
{
    CGPoint translation = [recognizer translationInView:self.thumbView.superview];
    if (recognizer.state == UIGestureRecognizerStateChanged)
    {
        [recognizer setTranslation:CGPointZero inView:self.thumbView.superview];
        CGRect newFrame = CGRectOffset(self.thumbView.frame, translation.x, 0);
        newFrame.origin.x = MAX(newFrame.origin.x, self.thumbInset);
        newFrame.origin.x = MIN(newFrame.origin.x, self.frameWidth - self.thumbInset - self.thumbView.frameWidth);
        self.thumbView.frame = newFrame;
    }
    if (recognizer.state == UIGestureRecognizerStateEnded)
    {
        CGFloat x = self.thumbView.center.x;
        BOOL newValue = (x > self.frameWidth / 2.0);
        BOOL old = self.isOn;
        [self setOn:newValue animated:YES];
        if (old != newValue)
        {
            [self sendActionsForControlEvents:UIControlEventValueChanged];
        }
    }
}

- (void)tap:(UIGestureRecognizer *)recognizer
{
    if (recognizer.state == UIGestureRecognizerStateEnded)
    {
        [self setOn:!self.isOn animated:YES];
        [self sendActionsForControlEvents:UIControlEventValueChanged];
    }
}

- (void)swipe:(UISwipeGestureRecognizer *)recognizer
{
    if (recognizer.state == UIGestureRecognizerStateEnded)
    {
        BOOL newValue = recognizer.direction != UISwipeGestureRecognizerDirectionLeft;
        if (self.isOn != newValue)
        {
            [self setOn:newValue animated:YES];
            [self sendActionsForControlEvents:UIControlEventValueChanged];
        }
    }
}

- (void)updateSwitch:(BOOL)on
{
    _on = on;
    [self sendActionsForControlEvents:UIControlEventValueChanged];
}

#pragma mark - colors && images

- (void)setThumbImage:(UIImage *)thumbImage
{
    self.thumbView.image = thumbImage;
}

- (void)setOnImage:(UIImage *)onImage
{
    self.onBackgroundView.image = onImage;
}

- (void)setOffImage:(UIImage *)offImage
{
    self.offBackgroundView.image = offImage;
}

- (void)setBackgroundImage:(UIImage *)image
{
    self.backgroundView.image = image;
}

- (void)setOnTintColor:(UIColor *)color
{
    _onTintColor = color;
    [self.onBackgroundView setBackgroundColor:color];
}

- (void)setOnTintBorderColor:(UIColor *)color
{
    _onTintBorderColor = color;
    [self.onBackgroundView.layer setBorderColor:color.CGColor];
    self.onBackgroundView.layer.borderWidth = nil == color ? 0.0f : kSwitchBorderWidth;
}

- (void)setOffTintColor:(UIColor *)color
{
    _offTintColor = color;
    [self.offBackgroundView setBackgroundColor:color];
}

- (void)setOffTintBorderColor:(UIColor *)color
{
    _offTintBorderColor = color;
    [self.offBackgroundView.layer setBorderColor:color.CGColor];
    self.offBackgroundView.layer.borderWidth = nil == color ? 0.0f : kSwitchBorderWidth;
}

- (void)setThumbTintColor:(UIColor *)color
{
    _thumbTintColor = color;
    [self.thumbView setBackgroundColor:color];
}

@end
