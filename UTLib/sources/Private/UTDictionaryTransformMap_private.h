//
//  UTDictionaryTransformMap.h
//  UTLib
//
//  Created by Maksym Kareta on 12/30/13.
//  Copyright (c) 2013 Maksym Kareta. All rights reserved.
//

#import "UTDictionaryTransformMap.h"

@interface UTDictionaryTransformMapEntry : NSObject

@property (strong, nonatomic) NSString *objectKey;
@property (strong, nonatomic) NSString *dictKey;
@property (copy, nonatomic) UTTransformMapBlock transform;
@property (copy, nonatomic) UTTransformMapUpdateBlock update;

@end

@interface UTDictionaryTransformMap()

- (NSArray *)entries;

@end

