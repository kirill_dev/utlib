//
//  UTDelegateArray.m
//  UTLib
//
//  Created by Maksym Kareta on 9/3/13.
//  Copyright (c) 2013 Maksym Kareta. All rights reserved.
//


#import "UTStorageProvider.h"

@interface UTStorageProvider()

@property (strong, nonatomic) NSManagedObjectModel *model;
@property (strong, nonatomic) NSPersistentStoreCoordinator *coordinator;
@property (strong, nonatomic) NSManagedObjectContext *context;
@property (strong, nonatomic) NSMutableArray *contextArray;
@property (strong, nonatomic) NSRecursiveLock *locker;
@property (strong, nonatomic) NSString *modelName;
@property (strong, nonatomic) NSString *fileName;

@end


@implementation UTStorageProvider

- (id)initWithModelName:(NSString *)modelName databaseFileName:(NSString *)fileName
{
	self = [super init];
	if (nil != self)
	{
        self.modelName = modelName;
        self.fileName = fileName;
        if (nil == self.fileName)
        {
            self.fileName = @"dataBase";
        }
		self.contextArray = [NSMutableArray array];
		self.locker = [NSRecursiveLock new];
		[self openContext];
	}
	return self;
    
}

- (id)init
{
    return [self initWithModelName:nil databaseFileName:nil];
}

#pragma mark -

- (NSManagedObjectModel *)createModel
{
    if (nil == self.modelName)
    {
        return [NSManagedObjectModel mergedModelFromBundles:nil];
    }
    
    NSString *modelPath = [[NSBundle mainBundle] pathForResource:self.modelName ofType:@"momd"];
    NSURL *modelURL = [NSURL fileURLWithPath:modelPath];
    NSManagedObjectModel *model = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return model;
}

- (void)openContext
{
	[self.locker lock];
	self.model = [self createModel];
	self.coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.model];
	NSError *error = nil;
	NSURL *url = [NSURL fileURLWithPath:[self databasePath]];
	NSDictionary *options = @{NSMigratePersistentStoresAutomaticallyOption: @(YES),NSInferMappingModelAutomaticallyOption: @(YES)};
	[self.coordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:url options:options error:&error];
    if (nil != error)
    {
        error = nil;
        [[NSFileManager defaultManager] removeItemAtPath:[self databasePath] error:nil];
        [self.coordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:url options:options error:&error];
        NSAssert(nil == error, @"");
    }
	self.context = [[NSManagedObjectContext alloc] init];
    [self.context setUndoManager:nil];
	[self.context setPersistentStoreCoordinator:self.coordinator];
	[self addContext:self.context];
	[self.locker unlock];
}

- (void)closeContext
{
	[self.locker lock];
	self.context = nil;
	[self.contextArray removeAllObjects];
	self.coordinator = nil;
	self.model = nil;
	[self.locker unlock];
}

- (void)removeDataBaseFile
{
	[self.locker lock];
	[[NSFileManager defaultManager] removeItemAtPath:[self databasePath] error:nil];
	[self.locker unlock];
}

- (void)removeDataBase
{
	[self closeContext];
	[self removeDataBaseFile];
	[self openContext];
}

- (NSString *)databasePath
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *path = [NSString stringWithFormat:@"%@/%@.%@", paths[0], self.fileName, @"sqlite"];
	return path;
}

- (NSManagedObjectModel *)objectModel
{
	return self.model;
}

- (NSPersistentStoreCoordinator *)storeCoordinator
{
	NSPersistentStoreCoordinator *cordinator = nil;
    [self.locker lock];
    cordinator = self.coordinator;
    [self.locker unlock];
	return cordinator;
}

- (NSManagedObjectContext *)objectContext
{
	return self.context;
}

- (void)addContext:(NSManagedObjectContext *)context
{
	[self.locker lock];
	[self.contextArray addObject:context];
	[self.locker unlock];
}

- (void)removeContext:(NSManagedObjectContext *)context
{
	[self.locker lock];
	NSUInteger index = [self.contextArray indexOfObject:context];
	UTAssert(NSNotFound != index, @"Try remove context that not in pool");
	[self.contextArray removeObjectAtIndex:index];
	[self.locker unlock];
}

- (NSManagedObjectContext *)newContext
{
	[self.locker lock];
	NSManagedObjectContext *context = [[NSManagedObjectContext alloc] init];
	[context setPersistentStoreCoordinator:self.coordinator];
	[context setUndoManager:nil];
	[self.locker unlock];
	[self addContext:context];
	return context;
}

- (NSArray *)allContexts
{
    [self.locker lock];
    NSArray *result = [self.contextArray copy];
    [self.locker unlock];
    return result;
}

@end
