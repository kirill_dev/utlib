//
//  NSManagedObjectContext+entityDescription.h
//  UTLib
//
//  Created by Maksym Kareta on 4/4/12.
//  Copyright (c) 2012 Maksym Kareta. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObjectContext (entityDescription)

- (NSEntityDescription *)entityForName:(NSString *)string;

@end
