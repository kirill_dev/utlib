//
//  NSManagedObjectContext+printSaveError.h
//  FRS
//
//  Created by Maksym Kareta on 1/23/12.
//  Copyright (c) 2012 Maksym Kareta. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObjectContext (saving)

///Should not be used in production
- (BOOL)saveAndPrintError;
- (NSString *)saveAndReturnErrorDescription;

@end
