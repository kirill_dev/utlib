//
//  NSManagedObject+UTMethods.h
//  UTLib
//
//  Created by Maksym Kareta on 2/4/15.
//  Copyright (c) 2015 Maksym Kareta. All rights reserved.
//

@import CoreData;

@interface NSManagedObject(UTMethods)

+ (instancetype)createInContext:(NSManagedObjectContext *)context;
- (BOOL)canBeUsed;

@end
