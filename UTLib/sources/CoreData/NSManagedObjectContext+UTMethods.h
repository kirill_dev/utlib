//
//  NSManagedObjectContext+entityDescription.h
//  UTLib
//
//  Created by Maksym Kareta on 8/31/12.
//  Copyright (c) 2012 Cogniance Inc. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObjectContext (UTMethods)

+ (NSManagedObjectContext *)inMemoryContextForTestingWithModelName:(NSString *)name bundle:(NSBundle *)bundle;
+ (NSManagedObjectContext *)inMemoryContextForTestingWithModelPath:(NSString *)path;

- (void)deleteObjects:(id/*NSArray, NSSet*/)obejcts;
- (NSEntityDescription *)entityForName:(NSString *)string;
- (NSManagedObjectModel *)model;
- (NSFetchRequest *)requestFromTemplateWithName:(NSString *)name attributes:(NSDictionary *)dict;
- (NSArray *)performRequestFromTemplateWithName:(NSString *)name attributes:(NSDictionary *)dict;

- (NSArray *)allObjectsWithEntityName:(NSString *)name;

@end
