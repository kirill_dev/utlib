//
//  NSManagerObject+UTMethods.m
//  UTLib
//
//  Created by Maksym Kareta on 2/4/15.
//  Copyright (c) 2015 Maksym Kareta. All rights reserved.
//

#import "NSManagedObject+UTMethods.h"

@implementation NSManagedObject(UTMethods)

+ (instancetype)createInContext:(NSManagedObjectContext *)context
{
    return [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass(self) inManagedObjectContext:context];
}

- (BOOL)canBeUsed
{
    return !self.isDeleted && nil != self.managedObjectContext;
}

@end
