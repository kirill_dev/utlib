//
//  NSManagedObjectContext+entityDescription.m
//  UTLib
//
//  Created by Maksym Kareta on 8/31/12.
//  Copyright (c) 2012 Cogniance Inc. All rights reserved.
//

#import "NSManagedObjectContext+UTMethods.h"

@implementation NSManagedObjectContext (UTMethods)

+ (NSManagedObjectContext *)inMemoryContextForTestingWithModelName:(NSString *)name bundle:(NSBundle *)bundle
{
    NSString *path = [bundle pathForResource:name ofType:@"momd"];
    return [self inMemoryContextForTestingWithModelPath:path];
}

+ (NSManagedObjectContext *)inMemoryContextForTestingWithModelPath:(NSString *)path
{
    NSManagedObjectModel *model = [[NSManagedObjectModel alloc] initWithContentsOfURL:[NSURL URLWithString:path]];
	NSPersistentStoreCoordinator *coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:model];
	NSError *error = nil;
	NSDictionary *options = @{NSMigratePersistentStoresAutomaticallyOption: @(YES),NSInferMappingModelAutomaticallyOption: @(YES)};
	[coordinator addPersistentStoreWithType:NSInMemoryStoreType configuration:nil URL:nil options:options error:&error];
	NSManagedObjectContext *context = [[NSManagedObjectContext alloc] init];
    [context setUndoManager:nil];
	[context setPersistentStoreCoordinator:coordinator];
    return context;
}

- (NSEntityDescription *)entityForName:(NSString *)string
{
	return [NSEntityDescription entityForName:string inManagedObjectContext:self];
}

- (NSManagedObjectModel *)model
{
    return self.persistentStoreCoordinator.managedObjectModel;
}

- (NSFetchRequest *)requestFromTemplateWithName:(NSString *)name attributes:(NSDictionary *)dict
{
    NSFetchRequest *request = nil;
    if (nil == dict)
    {
        request = [[self.model fetchRequestTemplateForName:name] copy];
    }
    else
    {
        request = [self.model fetchRequestFromTemplateWithName:name substitutionVariables:dict];
    }
    return request;
}

- (NSArray *)performRequestFromTemplateWithName:(NSString *)name attributes:(NSDictionary *)dict
{
    NSFetchRequest *request = [self requestFromTemplateWithName:name attributes:dict];
    return [self executeFetchRequest:request error:nil];
}

- (void)deleteObjects:(id/*NSArray, NSSet*/)obejcts
{
    for (NSManagedObject *object in obejcts)
    {
        [self deleteObject:object];
    }
}

- (NSArray *)allObjectsWithEntityName:(NSString *)name
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:name];
    return [self executeFetchRequest:request error:nil];
}

@end
