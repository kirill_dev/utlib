//
//  UTDelegateArray.m
//  UTLib
//
//  Created by Maksym Kareta on 9/3/13.
//  Copyright (c) 2013 Maksym Kareta. All rights reserved.
//

#import <CoreData/CoreData.h>

/// This class completely thread safe
@interface UTStorageProvider : NSObject

///Create new instance of UTStorageProvider
///@param modelName Name of model. UTStorageProvider will search for modelName.momd in resourse. If nil mergedModelFromBundles will be used.
///@param fileName Name of database file. Default is @"dataBase".
- (id)initWithModelName:(NSString *)modelName databaseFileName:(NSString *)fileName;

/// Return defult context.
///
/// All contexts will update main context after saving.
- (NSManagedObjectContext *)objectContext;

/// Create and return new context.
///
/// New context added into context pool and must be removed after using with removeContext: method.
/// NOTE: try to avoid creating context manually.
- (NSManagedObjectContext *)newContext;

/// Removes context from pool.
/// @note: allways invoke this method if context was created with newContext.
/// @param context Context that should be removed.
- (void)removeContext:(NSManagedObjectContext *)context;

/// This mehtod must be overwritten.
/// Default implementation return merged model from main bundle.
- (NSManagedObjectModel *)createModel;

/// This mehtod must be overwritten.
/// Default implementation returns <Documents>/dataBase.sqlite .
- (NSString *)databasePath;

/// Return all contexts that currently in pool.
- (NSArray *)allContexts;

@end
