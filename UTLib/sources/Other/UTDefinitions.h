//
//  UTDefinitions.h
//  UsefulThings
//
//  Created by Maksym Kareta on 3/8/12.
//  Copyright (c) 2012 Maksym Kareta. All rights reserved.
//

#ifdef UT_DEBUG

#define UTAssert(condition, desc, ...) \
do {			\
if (!(condition)) {	\
[[NSAssertionHandler currentHandler] handleFailureInMethod:_cmd \
object:self file:[NSString stringWithUTF8String:__FILE__] \
lineNumber:__LINE__ description:(desc), ##__VA_ARGS__]; \
}			\
} while(0)

#define UT_DEBUG_BEGIN do {
#define UT_DEBUG_END } while(0)

#define UTLOG( s, ... ) NSLog( @"<%p %@:(%d) %s> %@", self, [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__, __FUNCTION__, [NSString stringWithFormat:(s), ##__VA_ARGS__] )

#else

#define UTAssert(condition, desc, ...)
#define UT_DEBUG_BEGIN while (0){
#define UT_DEBUG_END }
#define UTLOG( s, ... )

#endif

