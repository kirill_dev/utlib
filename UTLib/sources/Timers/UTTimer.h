//
//  UTTimer.h
//  UTLib
//
//  Created by Maksym Kareta on 9/18/12.
//  Copyright (c) 2012 Maksym Kareta. All rights reserved.
//

/*
 * Uses NSTimer underhood
 */

typedef void (^UTFireBlock)(BOOL *stop);

@interface UTTimer : NSObject

/// Returns a Boolean value that indicates whether the receiver is currently valid.
/// @return YES if the receiver is still capable of firing or NO if the timer has been invalidated and is no longer capable of firing.
@property (assign, nonatomic, getter = isValid, readonly) BOOL valid;

/// @see NNSTimer documentation
@property (assign, nonatomic, readonly) NSTimeInterval timeInterval;

#ifdef __IPHONE_7_0
/// @see NSTimer documentation
@property (assign, nonatomic) NSTimeInterval tolerance NS_AVAILABLE_IOS(7_0);
#endif

/// Creates and returns a new UTTimer object and schedules it on the current run loop in the default mode.
/// After "seconds" seconds have elapsed, the timer fires, calling fireBlock.
///
/// @param seconds The number of seconds between firings of the timer. If seconds is less than or equal to 0.0, this method chooses the nonnegative value of 0.1 milliseconds instead.
/// @param repeats If YES, the timer will repeatedly reschedule itself until invalidated. If NO, the timer will be invalidated after it fires.
/// @param block Block to fire. Block will copied.
/// @returns A new UTTimer object, configured according to the specified parameters.
+ (UTTimer *)scheduledTimerWithTimeInterval:(NSTimeInterval)seconds repeats:(BOOL)repeats fireBlock:(UTFireBlock)block;

/// Ivalidate timer and release block
- (void)invalidate;

/// Schedules receiver on the current run loop in the default mode.
/// After "seconds" seconds have elapsed, the timer fires, calling fireBlock.
///
/// @param seconds The number of seconds between firings of the timer. If seconds is less than or equal to 0.0, this method chooses the nonnegative value of 0.1 milliseconds instead.
/// @param repeats If YES, the timer will repeatedly reschedule itself until invalidated. If NO, the timer will be invalidated after it fires.
/// @param block Block to fire. Block will copied.
/// @warning Call invalidate before start
- (void)scheduleWithTimeInterval:(NSTimeInterval)seconds repeats:(BOOL)repeats fireBock:(UTFireBlock)block;

/// Reschedules receiver on the current run loop in the default mode with new repeat interval.
/// @param seconds The number of seconds between firings of the timer. If seconds is less than or equal to 0.0, this method chooses the nonnegative value of 0.1 milliseconds instead.
/// @note Do nothing if timer don't repeats or invalid.
/// @note Next fire will be not earlier than "seconds" seconds since now.
/// @note Do nothing if seconds equal to current timeInterval
- (void)rescheduleWithTimerInterval:(NSTimeInterval)seconds;

@end
