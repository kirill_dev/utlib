//
//  UTGCDTimer.m
//  UTLib
//
//  Created by Maksym Kareta on 10/24/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

#import "UTGCDTimer.h"

@interface UTGCDTimer()

@property (assign, nonatomic) BOOL repeat;
@property (strong, nonatomic) UTFireBlock block;
@property (strong, nonatomic) dispatch_source_t timer;
@property (assign, nonatomic) int64_t interval;
@property (assign, nonatomic) int64_t leeway;

@end

@implementation UTGCDTimer

+ (UTGCDTimer *)scheduledTimerInQueue:(dispatch_queue_t)queue withTimeInterval:(NSTimeInterval)seconds repeats:(BOOL)repeats leewayTimeInterval:(NSTimeInterval)leewaySeconds fireBlock:(UTFireBlock)block
{
    int64_t inteval = (int64_t)(seconds * NSEC_PER_SEC);
    int64_t leeway = (int64_t)(leewaySeconds * NSEC_PER_SEC);
    return [self scheduledTimerInQueue:queue withInterval:inteval repeats:repeats leeway:leeway fireBlock:block];
}

+ (UTGCDTimer *)scheduledTimerInQueue:(dispatch_queue_t)queue withInterval:(int64_t)interval repeats:(BOOL)repeats leeway:(int64_t)leeway fireBlock:(UTFireBlock)block
{
    UTGCDTimer *result = [UTGCDTimer new];
    result.block = block;
    result.repeat = repeats;
    result.interval = interval;
    result.leeway = leeway;
    
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    result.timer = timer;

    dispatch_time_t now = dispatch_time(DISPATCH_TIME_NOW, interval);
    
    dispatch_source_set_timer(timer, now, interval, leeway);
    dispatch_source_set_event_handler(timer, ^{
        BOOL stop = !result.repeat;
        result.block(&stop);
        if (stop)
        {
            dispatch_source_cancel(result.timer);
        }
    });
    dispatch_source_set_cancel_handler(timer, ^{
        result.block = NULL;
        result.timer = NULL;
    });
    dispatch_resume(timer);
    return result;
}

+ (UTGCDTimer *)scheduledTimerInQueue:(dispatch_queue_t)queue withTimeInterval:(NSTimeInterval)seconds repeats:(BOOL)repeats fireBlock:(UTFireBlock)block
{
    return [self scheduledTimerInQueue:queue withTimeInterval:seconds repeats:repeats leewayTimeInterval:0 fireBlock:block];
}

- (NSTimeInterval)timeInterval
{
    return (NSTimeInterval)self.interval / NSEC_PER_SEC;
}

- (void)invalidate
{
    dispatch_source_cancel(self.timer);
}

- (void)reschedule
{
    dispatch_time_t now = dispatch_time(DISPATCH_TIME_NOW, self.interval);
    dispatch_source_set_timer(self.timer, now, self.interval, self.leeway);
}

- (void)rescheduleWithInterval:(NSTimeInterval)seconds
{
    self.interval = (int64_t)(seconds * NSEC_PER_SEC);
    [self reschedule];
}

@end
