//
//  UTTimer.h
//  UTLib
//
//  Created by Oleg Zinko on 6/18/12.
//  Copyright (c) 2012 Oleg Zinko. All rights reserved.
//


/// UTGameTimer provide functionality for time measurements while game app is active.
///
/// UTGameTimer automaticly perform pause action when application go to backgroud.
/// Timer also protected from simple time changing by saving state when app go background.
/// @note You should manually resume timer when app became active.
/// @warning Timer if self retained while working, you should stop it before release.
/// @warning Timer is not designed for precise measurements.
@interface UTGameTimer : NSObject

/// Is paused
@property (assign, nonatomic, readonly, getter = isPaused) BOOL paused;

/// Time passed since start called
@property (assign, nonatomic, readonly) NSTimeInterval passedTime;

/// Start/Resume timer
- (void)startTimer;

/// Pause timer
- (void)pauseTimer;

/// Stop timer
///
/// @note Passed time became 0
- (void)stopTimer;

@end
