//
//  UTGCDTimer.h
//  UTLib
//
//  Created by Maksym Kareta on 10/24/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

typedef void (^UTFireBlock)(BOOL *stop);

@interface UTGCDTimer : NSObject

@property (assign, nonatomic, readonly) NSTimeInterval timeInterval;

+ (UTGCDTimer *)scheduledTimerInQueue:(dispatch_queue_t)queue withTimeInterval:(NSTimeInterval)seconds repeats:(BOOL)repeats leewayTimeInterval:(NSTimeInterval)leewaySeconds fireBlock:(UTFireBlock)block;
+ (UTGCDTimer *)scheduledTimerInQueue:(dispatch_queue_t)queue withInterval:(int64_t)interval repeats:(BOOL)repeats leeway:(int64_t)leeway fireBlock:(UTFireBlock)block;
+ (UTGCDTimer *)scheduledTimerInQueue:(dispatch_queue_t)queue withTimeInterval:(NSTimeInterval)seconds repeats:(BOOL)repeats fireBlock:(UTFireBlock)block;

- (void)invalidate;
- (void)reschedule;
- (void)rescheduleWithInterval:(NSTimeInterval)newInteval;

@end
