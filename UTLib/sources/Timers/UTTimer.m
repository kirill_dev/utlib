//
//  UTTimer.m
//  UTLib
//
//  Created by Maksym Kareta on 9/18/12.
//  Copyright (c) 2012 Maksym Kareta. All rights reserved.
//

#import "UTTimer.h"

@interface UTTimer()

@property (copy, nonatomic) UTFireBlock fireBlock;
@property (strong, nonatomic) NSTimer *timer;
@property (assign, nonatomic) BOOL repeats;

@end


@implementation UTTimer
#ifdef __IPHONE_7_0
@synthesize tolerance = _tolerance;
#endif

+ (UTTimer *)scheduledTimerWithTimeInterval:(NSTimeInterval)ti repeats:(BOOL)yesOrNo fireBlock:(UTFireBlock)block
{
    UTTimer *result = [UTTimer new];
    [result scheduleWithTimeInterval:ti repeats:yesOrNo fireBock:block];
    return result;
}

- (void)dealloc
{
    [self.timer invalidate];
}

- (NSTimeInterval)timeInterval
{
    return self.timer.timeInterval;
}

- (void)scheduleWithTimeInterval:(NSTimeInterval)ti repeats:(BOOL)yesOrNo fireBock:(UTFireBlock)block
{
    NSAssert(NULL != block, @"fire block can't be null");
    [self invalidate];
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:ti target:self selector:@selector(fire:) userInfo:nil repeats:yesOrNo];
#ifdef __IPHONE_7_0
    [timer setTolerance:_tolerance];
#endif
    [self.timer invalidate];
    self.timer = timer;
    self.repeats = yesOrNo;
    self.fireBlock = block;
}

- (void)rescheduleWithTimerInterval:(NSTimeInterval)ti
{
    if (self.repeats && self.isValid && self.timeInterval != ti)
    {
        NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:ti target:self selector:@selector(fire:) userInfo:nil repeats:YES];
#ifdef __IPHONE_7_0
        [timer setTolerance:_tolerance];
#endif
        [self.timer invalidate];
        self.timer = timer;
    }
}

- (void)fire:(id)sender
{
    BOOL stop = NO;
    self.fireBlock(&stop);
    if (stop || !self.repeats)
    {
        [self invalidate];
    }
}

- (void)invalidate
{
	[self.timer invalidate];
    self.timer = nil;
    self.fireBlock = nil;
}

- (BOOL)isValid
{
    return (self.timer != nil);
}

#ifdef __IPHONE_7_0
- (NSTimeInterval)tolerance
{
    return _tolerance;
}

- (void)setTolerance:(NSTimeInterval)tolerance
{
    _tolerance = tolerance;
    [self.timer setTolerance:tolerance];
}
#endif

@end
