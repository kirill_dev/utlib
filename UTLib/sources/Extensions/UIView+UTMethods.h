//
//  UIView+UTMethods.h
//  UsefulThings
//
//  Created by Maksym Kareta on 3/8/12.
//  Copyright (c) 2012 Maksym Kareta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (UTMethods)

+ (CGRect)fullFrameForView:(UIView *)view;
- (CGRect)fullFrame;

///Recursive lookup for first responder
- (UIResponder *)firstResponder;

///Disable multiple touch for view and all subviews
///@note: setMultipleTouchEnabled:NO and setExclusiveTouch:YES
- (void)disableMultipleTouch;

- (void)disableTouchDelayRecursive:(BOOL)recursive;

//following methods will set new frame
- (void)setFrameWidth:(CGFloat)width;
- (void)setFrameHeight:(CGFloat)height;
- (void)setFrameOriginX:(CGFloat)x;
- (void)setFrameOriginY:(CGFloat)x;
- (void)setFrameSize:(CGSize)size;
- (void)setFrameOrigin:(CGPoint)origin;

- (CGFloat)frameWidth;
- (CGFloat)frameHeight;
- (CGFloat)frameOriginX;
- (CGFloat)frameOriginY;
- (CGSize)frameSize;
- (CGPoint)frameOrigin;
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wincomplete-implementation"
- (NSString *)recursiveDescription;
#pragma clang diagnostic pop

@end

CG_EXTERN CGRect UTCGRectMake(CGPoint origin, CGSize size);

