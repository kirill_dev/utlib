//
//  NSObject+DictionaryTransform.m
//  UTLib
//
//  Created by Maksym Kareta on 3/18/15.
//  Copyright (c) 2015 Maksym Kareta. All rights reserved.
//

#import "NSObject+DictionaryTransform.h"
#import "UTDictionaryTransformMap_private.h"
#import "GCD+UTFunctions.h"

@implementation NSObject(DictionaryTransform)

- (NSDictionary *)dictionaryRepresentaionWithTransformMap:(UTDictionaryTransformMap *)map
{
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    
    for (UTDictionaryTransformMapEntry *entry in [map entries])
    {
        if (nil != entry.transform)//complex case
        {
            NSDictionary *tempDict = entry.transform(self, entry.objectKey);
            [result addEntriesFromDictionary:tempDict];
        }
        else if (entry.dictKey.length > 0)
        {
            NSAssert(entry.objectKey.length > 0, @"");
            NSArray *path = [entry.dictKey componentsSeparatedByString:@"."];
            if (path.count == 1)
            {
                id obj = [self valueForKeyPath:entry.objectKey];
                if (nil != obj)
                {
                    result[entry.dictKey] = obj;
                }
            }
            else //keypath
            {
                NSMutableDictionary *currentDict = result;
                for (NSString *part in path)
                {
                    NSMutableDictionary *next = currentDict[part];
                    if (nil == next)
                    {
                        next = [NSMutableDictionary dictionary];
                        currentDict[part] = next;
                    }
                    currentDict = next;
                }
                id obj = [self valueForKeyPath:entry.objectKey];
                if (nil != obj)
                {
                    [result setValue:obj forKeyPath:entry.dictKey];
                }
            }
        }
    }
    return result;
}

- (void)updateWithDicitonary:(NSDictionary *)dict transformMap:(UTDictionaryTransformMap *)map
{
    for (UTDictionaryTransformMapEntry *entry in [map entries])
    {
        if (nil != entry.update)
        {
            UTInvokeBlock(entry.update, self, entry.objectKey, dict);
        }
        else if (entry.dictKey.length > 0)
        {
            NSAssert(entry.objectKey.length > 0, @"");
            id val = [dict valueForKeyPath:entry.dictKey];
            if (nil != val)
            {
                [self setValue:val forKeyPath:entry.objectKey];
            }
        }
    }
}

- (NSDictionary *)dictionaryRepresentation
{
    return [self dictionaryRepresentaionWithTransformMap:self.dictionaryTransformMap];
}

- (UTDictionaryTransformMap *)dictionaryTransformMap
{
    NSAssert(NO, @"overwrite dictionaryTransformMap or updateWithDicitonary and dictionaryRepresentaion");
    return nil;
}

- (void)updateWithDictionary:(NSDictionary *)dict
{
    [self updateWithDicitonary:dict transformMap:self.dictionaryTransformMap];
}

@end
