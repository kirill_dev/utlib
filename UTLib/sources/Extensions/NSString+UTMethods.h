//
//  NSString+UTMethods.h
//  UsefulThings
//
//  Created by Maksym Kareta on 3/8/12.
//  Copyright (c) 2012 Maksym Kareta. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (UTMethods)

/// Create string with UUID.
///
/// @discussion Create and return newly generated UUID
/// @returns NSString with new UUID
+ (NSString *)UUID;

@end

/// Create string with provided number.
///
/// @discussion Use [NSString stringWithFormat:] to generate string with number
/// @param val Number that should be converted to string
/// @returns NSString with converted number
extern NSString *NSStringFromInt(long val);

/// Create string with provided number.
///
/// @discussion Use [NSString stringWithFormat:] to generate string with number
/// @param val Number that should be converted to string
/// @returns NSString with converted number
extern NSString *NSStringFromLong(long val);

/// Create string with provided number.
///
/// @discussion Use [NSString stringWithFormat:] to generate string with number
/// @param val Number that should be converted to string
/// @returns NSString with converted number
extern NSString *NSStringFromFloat(float val);

/// Create string with provided number.
///
/// @discussion Use [NSString stringWithFormat:] to generate string with number
/// @param val Number that should be converted to string
/// @returns NSString with converted number
extern NSString *NSStringFromDouble(double val);

/// Create string with provided number.
///
/// @discussion Use [NSString stringWithFormat:@"%.2lf"] to generate string with number
/// @param val Number that should be converted to string
/// @returns NSString with converted number
extern NSString *NSStringFromDouble2(double val);

/// Create string with provided number.
///
/// @discussion Use [NSString stringWithFormat:@"%.0lf"] to generate string with number
/// @param val Number that should be converted to string
/// @returns NSString with converted number
extern NSString *NSStringFromDouble0(double val);


extern NSString * __attribute__((overloadable)) NSStringFromNumber(const float val);
extern NSString * __attribute__((overloadable)) NSStringFromNumber(const double val);

extern NSString * __attribute__((overloadable)) NSStringFromNumber(const int8_t val);
extern NSString * __attribute__((overloadable)) NSStringFromNumber(const int16_t val);
extern NSString * __attribute__((overloadable)) NSStringFromNumber(const int32_t val);
extern NSString * __attribute__((overloadable)) NSStringFromNumber(const int64_t val);

extern NSString * __attribute__((overloadable)) NSStringFromNumber(const u_int8_t val);
extern NSString * __attribute__((overloadable)) NSStringFromNumber(const u_int16_t val);
extern NSString * __attribute__((overloadable)) NSStringFromNumber(const u_int32_t val);
extern NSString * __attribute__((overloadable)) NSStringFromNumber(const u_int64_t val);

