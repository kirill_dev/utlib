//
//  NSArray+UTMethods.m
//  UTLib
//
//  Created by Maksym Kareta on 6/11/12.
//  Copyright (c) 2012 Maksym Kareta. All rights reserved.
//

#import "NSArray+UTMethods.h"

@implementation NSArray (UTMethods)

- (void)makeObjectsPerformSelector:(SEL)aSelector withObject:(id)firstObject secondObejct:(id)secondObject
{
    [self enumerateObjectsUsingBlock:^(NSObject *obj, NSUInteger idx, BOOL *stop) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [obj performSelector:aSelector withObject:firstObject withObject:secondObject];
#pragma clang diagnostic pop
    }];
}

- (NSArray *)sortedArrayUsingDescriptor:(NSSortDescriptor *)desciptor
{
    return [self sortedArrayUsingDescriptors:@[desciptor]];
}

- (NSArray *)sortedArrayUsingKey:(NSString *)key ascending:(BOOL)ascending
{
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:key ascending:ascending];
    return [self sortedArrayUsingDescriptors:@[descriptor]];
}

- (NSArray *)sortedArrayUsingKey:(NSString *)key
{
    return [self sortedArrayUsingKey:key ascending:YES];
}

@end

@implementation NSMutableArray (UTMethods)

- (void)addObjectIfNotNil:(id)obj
{
    if (nil != obj)
    {
        [self addObject:obj];
    }
}

- (void)sortArrayUsingDescriptor:(NSSortDescriptor *)desciptor
{
    [self sortUsingDescriptors:@[desciptor]];
}

- (void)sortArrayUsingKey:(NSString *)key ascending:(BOOL)ascending
{
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:key ascending:ascending];
    [self sortUsingDescriptors:@[descriptor]];
}

- (void)sortArrayUsingKey:(NSString *)key
{
    [self sortArrayUsingKey:key ascending:YES];
}

@end
