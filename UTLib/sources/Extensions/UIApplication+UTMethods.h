//
//  UIApplication+UTMethods.h
//  UTLib
//
//  Created by Maksym Kareta on 1/2/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

#import "UTDefinitionsC.h"

@import CoreLocation;

UT_EXTERN UIApplication *UTSharedApplication(void);

@interface UIApplication(UTMethods)

+ (void)showDirectionsWithAppleMapsFrom:(CLLocationCoordinate2D)start to:(CLLocationCoordinate2D)stop;

@end
