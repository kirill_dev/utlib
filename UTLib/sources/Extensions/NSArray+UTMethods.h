//
//  NSArray+UTMethods.h
//  UTLib
//
//  Created by Maksym Kareta on 6/11/12.
//  Copyright (c) 2012 Maksym Kareta. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (UTMethods)

- (void)makeObjectsPerformSelector:(SEL)aSelector withObject:(id)firstObject secondObejct:(id)secondObject;

- (NSArray *)sortedArrayUsingDescriptor:(NSSortDescriptor *)desciptor;
- (NSArray *)sortedArrayUsingKey:(NSString *)key ascending:(BOOL)ascending;
- (NSArray *)sortedArrayUsingKey:(NSString *)key;

@end

@interface NSMutableArray (UTMethods)

- (void)addObjectIfNotNil:(id)obj;
- (void)sortArrayUsingDescriptor:(NSSortDescriptor *)desciptor;
- (void)sortArrayUsingKey:(NSString *)key ascending:(BOOL)ascending;
- (void)sortArrayUsingKey:(NSString *)key;


@end
