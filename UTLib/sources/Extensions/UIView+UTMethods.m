//
//  UIView+UTMethods.m
//  UsefulThings
//
//  Created by Maksym Kareta on 3/8/12.
//  Copyright (c) 2012 Maksym Kareta. All rights reserved.
//

#import "UIView+UTMethods.h"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wincomplete-implementation"

@implementation UIView (UTMethods)

#pragma clang diagnostic pop

+ (CGRect)fullFrameForView:(UIView *)view
{
    return CGRectMake(0, 0, view.frameWidth, view.frameHeight);
}

- (CGRect)fullFrame
{
    return CGRectMake(0, 0, self.frameWidth, self.frameHeight);
}

- (void)disableMultipleTouch
{
	[self setMultipleTouchEnabled:NO];
	[self setExclusiveTouch:YES];
	for (UIView *subview in [self subviews])
	{
		[subview disableMultipleTouch];
	}
}

- (void)disableTouchDelayRecursive:(BOOL)recursive
{
    for (UIView *currentView in self.subviews)
    {
        if([currentView isKindOfClass:[UIScrollView class]])
        {
            ((UIScrollView *)currentView).delaysContentTouches = NO;
            break;
        }
        if (recursive)
        {
            [currentView disableTouchDelayRecursive:recursive];
        }
    }
}

- (void)setFrameWidth:(CGFloat)width
{
    CGRect frame = [self frame];
	[self setFrame:CGRectMake(frame.origin.x, frame.origin.y, width, self.frame.size.height)];
}

- (void)setFrameHeight:(CGFloat)height
{
    CGRect frame = [self frame];
	[self setFrame:CGRectMake(frame.origin.x, frame.origin.y, self.frame.size.width, height)];
}

- (void)setFrameOriginX:(CGFloat)x
{
    CGRect frame = [self frame];
	[self setFrame:CGRectMake(x, frame.origin.y, self.frame.size.width, self.frame.size.height)];
}

- (void)setFrameOriginY:(CGFloat)y
{
    CGRect frame = [self frame];
	[self setFrame:CGRectMake(frame.origin.x, y, self.frame.size.width, self.frame.size.height)];
}

- (void)setFrameSize:(CGSize)size
{
    CGRect frame = [self frame];
    frame.size = size;
    self.frame = frame;
}

- (void)setFrameOrigin:(CGPoint)origin
{
    CGRect frame = [self frame];
    frame.origin = origin;
    self.frame = frame;
}

- (CGFloat)frameWidth
{
    return self.frame.size.width;
}

- (CGFloat)frameHeight
{
    return self.frame.size.height;
}

- (CGFloat)frameOriginX
{
    return self.frame.origin.x;
}

- (CGFloat)frameOriginY
{
    return self.frame.origin.y;
}

- (CGSize)frameSize
{
    return self.frame.size;
}

- (CGPoint)frameOrigin
{
    return self.frame.origin;
}

- (UIResponder *)firstResponder
{
    UIResponder *result = [self isFirstResponder] ? self : nil;
    if (nil == result)
    {
        for (UIView *view in self.subviews)
        {
            result = [view firstResponder];
            if (nil != result)
            {
                break;
            }
        }
    }
    return result;
}

@end

CGRect UTCGRectMake(CGPoint origin, CGSize size)
{
    CGRect rect;
    rect.origin = origin;
    rect.size = size;
    return rect;
}
