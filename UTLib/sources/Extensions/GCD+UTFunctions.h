//
//  GCD+UTFunctions.h
//  UTLib
//
//  Created by Maksym Kareta on 10/15/13.
//  Copyright (c) 2013 Maksym Kareta. All rights reserved.
//


typedef void (^UTErrorBlock)(NSError *error);
typedef void (^UTCompletionBlock)(id obj, NSError *error);
typedef void (^UTBlockWithObject)(id obj);
typedef BOOL (^UTBoolBlockWithObject)(id obj);
typedef id (^UTProcessObjectBlock)(id obj);
typedef void (^UTEmptyBlock)(void);

#define UTInvokeBlock(X, ...) do { if (NULL != X) {  \
X(__VA_ARGS__); \
} }while(0)


#define UTInvokeBlockOnMainQueue(X, ...) do { if (NULL != X) {  \
dispatch_async(dispatch_get_main_queue(), ^{ \
X(__VA_ARGS__);\
});\
} }while(0)

/// Enqueue a block for execution at the specified time.
/// @discussion This function waits until the specified time and then asynchronously adds block to the main queue.
/// @param delay - delay in seconds
/// @param block - block to execute, can't be NULL
DISPATCH_EXPORT DISPATCH_NONNULL2 void dispatch_with_delay(NSTimeInterval delay, dispatch_block_t block);

/// Enqueue a block for execution on main queue.
/// @discussion This function asynchronously adds block to the main queue.
/// @param block - block to execute, can't be NULL
DISPATCH_EXPORT DISPATCH_NONNULL1 void dispatch_async_on_main_queue(dispatch_block_t block);


