//
//  NSObject+DictionaryTransform.h
//  UTLib
//
//  Created by Maksym Kareta on 3/18/15.
//  Copyright (c) 2015 Maksym Kareta. All rights reserved.
//

@class UTDictionaryTransformMap;

@protocol UTDictionaryTransform <NSObject>

@required
- (UTDictionaryTransformMap *)dictionaryTransformMap;

@end


@interface NSObject(DictionaryTransform)

///Create dictionary from object using provide mapping dictionary.
///@param map UTDictionaryTransformMap that represents transform map.
///@return Dictionary that represents object.
- (NSDictionary *)dictionaryRepresentaionWithTransformMap:(UTDictionaryTransformMap *)map;

///Update object usign dictionary and provided map.
///@param map UTDictionaryTransformMap that represents transform map.
///@param dict Dictionary that represents object.
- (void)updateWithDicitonary:(NSDictionary *)dict transformMap:(UTDictionaryTransformMap *)map;

///Convenience method. Use dictionaryTransformMap as map for dictionaryRepresentaionWithTransformMap:
///@see dictionaryRepresentaionWithTransformMap:
- (NSDictionary *)dictionaryRepresentation;

///Convenience method. Use dictionaryTransformMap as map for updateWithDicitonary:transformMap:
///@see updateWithDicitonary:transformMap:
- (void)updateWithDictionary:(NSDictionary *)dict;

@end
