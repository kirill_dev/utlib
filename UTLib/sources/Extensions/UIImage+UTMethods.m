//
//  UIImage+UTMethods.m
//  UTLib
//
//  Created by Maksym Kareta on 3/28/15.
//  Copyright (c) 2015 Maksym Kareta. All rights reserved.
//

#import "UIImage+UTMethods.h"

@implementation UIImage(UTMethods)

- (UIImage *)mergeWithImage:(UIImage *)image
{
    NSParameterAssert(self.size.width >= image.size.width);
    NSParameterAssert(self.size.height >= image.size.height);
    
    UIGraphicsBeginImageContextWithOptions(self.size, NO, 0);
    [self drawInRect:CGRectMake(0, 0, self.size.width, image.size.height)];
    CGFloat xDiff = self.size.width - image.size.width;
    CGFloat yDiff = self.size.height - image.size.height;
    CGRect rect = CGRectMake(xDiff / 2.0f, yDiff / 2.0f, image.size.width, image.size.height);
    [image drawInRect:rect blendMode:kCGBlendModeNormal alpha:1.0];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+ (UIImage *)imageByMergingImage:(UIImage *)image1 withImage:(UIImage *)image2
{
    return [image1 mergeWithImage:image2];
}

- (UIImage *)tintWithColor:(UIColor *)tintColor
{
    UIGraphicsBeginImageContextWithOptions(self.size, NO, 0);
    CGRect drawRect = CGRectMake(0, 0, self.size.width, self.size.height);
    [self drawInRect:drawRect];
    [tintColor set];
    UIRectFillUsingBlendMode(drawRect, kCGBlendModeSourceAtop);
    UIImage *tintedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return tintedImage;
}

+ (UIImage *)clearImageWithSize:(CGSize)size
{
    UIGraphicsBeginImageContextWithOptions(size, NO, 0);
    CGRect drawRect = CGRectMake(0, 0, size.width, size.height);
    CGContextClearRect(UIGraphicsGetCurrentContext(), drawRect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

@end
