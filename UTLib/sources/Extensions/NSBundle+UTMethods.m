//
//  NSBundle+UTMethods.m
//  UTLib
//
//  Created by Olena Kachura on 11/24/13.
//  Copyright (c) 2013 Maksym Kareta. All rights reserved.
//

#import "NSBundle+UTMethods.h"

@implementation NSBundle(UTMethods)

+ (NSArray *)loadNibNamed:(NSString *)name owner:(id)owner
{
    return [[NSBundle mainBundle] loadNibNamed:name owner:owner options:nil];
}

@end
