//
//  UIColor+UTMethods.m
//  UTLib
//
//  Created by Maksym Kareta on 10/15/13.
//  Copyright (c) 2013 Maksym Kareta. All rights reserved.
//

#import "UIColor+UTMethods.h"

@implementation UIColor(UTMethods)

- (NSNumber *)RGBAHexNumber
{
    return [NSNumber numberWithInt:[self RGBAHexValue]];
}

- (u_int32_t)RGBAHexValue
{
    CGFloat r = 0;
    CGFloat g = 0;
    CGFloat b = 0;
    CGFloat a = 0;
    [self getRed:&r green:&g blue:&b alpha:&a];
    return ((((u_int32_t)(r * 255)) << (3*8)) | (((u_int32_t)(g * 255)) << (2*8)) |
            (((u_int32_t)(b * 255)) << (1*8)) | (((u_int32_t)(a * 255)) << (0*8)));
}

@end

UIColor *UTRGBAColor(NSInteger r, NSInteger g, NSInteger b, NSInteger a)
{
    return [UIColor colorWithRed:r / 255.0f green:g / 255.0f blue:b / 255.0f alpha:a / 255.0f];
}

UIColor *UTRGBColor(NSInteger r, NSInteger g, NSInteger b)
{
    return [UIColor colorWithRed:r / 255.0f green:g / 255.0f blue:b / 255.0f alpha:1.0];
}

UIColor *UTRGBAColorWithHexNumber(NSNumber *number)
{
    return (nil == number) ? nil : UTRGBAColorWithHex(number.intValue);
}

UIColor *UTRGBAColorWithHex(uint32_t hex)
{
    NSInteger r = ((hex & 0xff000000) >> (3*8));
    NSInteger g = ((hex & 0x00ff0000) >> (2*8));
    NSInteger b = ((hex & 0x0000ff00) >> (1*8));
    NSInteger a = ((hex & 0x000000ff) >> (0*8));
    return UTRGBAColor(r, g, b, a);
}
