//
//  UIAlertView+PresentError.m
//  SportStream
//
//  Created by Maksym Kareta on 8/3/12.
//  Copyright (c) 2012 Maksym Kareta. All rights reserved.
//

#import "UIAlertView+UTMethods.h"

@implementation UIAlertView (UTMethods)

+ (void)presentError:(NSError *)error okButtonTitle:(NSString *)okButtonTitle
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[error localizedFailureReason] message:[error localizedDescription] delegate:nil cancelButtonTitle:okButtonTitle otherButtonTitles:nil];
	[alert show];
}

+ (void)presentError:(NSError *)error
{
	[self presentError:error okButtonTitle:@"OK"];
}

+ (void)showText:(NSString *)text
{
    [[[UIAlertView alloc] initWithTitle:@"" message:text delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

@end
