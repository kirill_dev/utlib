//
//  NSDateFormatter+UTMethods.h
//  UTLib
//
//  Created by Maksym Kareta on 8/22/13.
//  Copyright (c) 2013 Maksym Kareta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSDateFormatter(UTMethods)

/// Create association between date formatter and ID
///
/// @param dateFormatter Date formatter that will be associated with "ID", can't be nil
/// @param ID Unique ID for dateFormatter, can't be nil
/// @note This method is thread safe
+ (void)registerFormatter:(NSDateFormatter *)dateFormatter withID:(NSString *)ID;

/// Return date formatter that was associated with ID
///
/// @param ID Unique ID for dateFormatter, can't be nil
/// @note This method is thread safe
/// @returns NSDateFormatter or nil if date formatter was not registered for this ID
+ (NSDateFormatter *)dateFromatterWithID:(NSString *)ID;

/// Release all date formatters
///
/// @note This method is thread safe
+ (void)purgeCaches;

@end
