//
//  NSLock_UTMethods.h
//  UTLib
//
//  Created by Maksym Kareta on 9/20/12.
//  Copyright (c) 2012 Maksym Kareta. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GCD+UTFunctions.h"

@interface NSLock (UTMethods)

/// Lock locker before execution block and unlock after
///
/// @param block Block that should be executed, can't be NULL
/// @param locker Object that support NSLocking protocol
+ (void)lockBlock:(UTEmptyBlock)block withLocker:(id <NSLocking>)locker;

/// Wait semaphore before execution block and signal after
///
/// @param block Block that should be executed, can't be NULL
/// @param sema GCD semaphore, can't be NULL
+ (void)lockBlock:(UTEmptyBlock)block withSemaphor:(dispatch_semaphore_t)sema;

@end
