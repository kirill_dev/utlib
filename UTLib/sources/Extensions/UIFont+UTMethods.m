//
//  UIFont+UTMethods.m
//  UTLib
//
//  Created by Maksym Kareta on 2/7/15.
//  Copyright (c) 2015 Maksym Kareta. All rights reserved.
//

#import "UIFont+UTMethods.h"

@implementation UIFont(UTMethods)

+ (void)applyFontWithFontName:(NSString *)name forAllLabelsInView:(UIView *)mainView includeSubviews:(BOOL)includeSubviews
{
    NSArray *subviews = [mainView subviews];
    for (UIView *view in subviews)
    {
        if ([view isKindOfClass:[UILabel class]] || [view isKindOfClass:[UITextView class]] || [view isKindOfClass:[UITextField class]])
        {
            [self changeFontForObject:view withFontName:name];
            continue;
        }
        if ([view isKindOfClass:[UIButton class]])
        {
            [self changeFontForObject:[(UIButton *)view titleLabel] withFontName:name];
            continue;
        }
        if (includeSubviews)
        {
            [self applyFontWithFontName:name forAllLabelsInView:view includeSubviews:includeSubviews];
        }
    }
}

+ (void)changeFontForObject:(id)obj withFontName:(NSString *)fontName
{
    if ([obj respondsToSelector:@selector(font)] && [obj respondsToSelector:@selector(setFont:)])
    {
        CGFloat size = [[obj font] pointSize];
        UIFont* newFont = [UIFont fontWithName:fontName size:size];
        [obj setFont:newFont];
    }
}

@end
