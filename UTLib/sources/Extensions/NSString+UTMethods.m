//
//  NSString+UTMethods.m
//  UsefulThings
//
//  Created by Maksym Kareta on 3/8/12.
//  Copyright (c) 2012 Maksym Kareta. All rights reserved.
//

#import "NSString+UTMethods.h"

@implementation NSString (UTMethods)

+ (NSString *)UUID
{
	CFUUIDRef uuidRef = CFUUIDCreate(kCFAllocatorDefault);
	CFStringRef stringRef = CFUUIDCreateString(kCFAllocatorDefault, uuidRef);
	CFRelease(uuidRef);
    NSString *string = CFBridgingRelease(stringRef);
	return string;
}

@end

NSString *NSStringFromInt(long val)
{
    return [NSString stringWithFormat:@"%ld", val];
}

NSString *NSStringFromLong(long val)
{
    return [NSString stringWithFormat:@"%ld", val];
}

NSString *NSStringFromFloat(float val)
{
    return [NSString stringWithFormat:@"%lf", val];
}

NSString *NSStringFromDouble(double val)
{
    return [NSString stringWithFormat:@"%lf", val];
}

NSString *NSStringFromDouble2(double val)
{
    return [NSString stringWithFormat:@"%.2lf", val];
}

NSString *NSStringFromDouble0(double val)
{
    return [NSString stringWithFormat:@"%.0lf", val];
}

NSString * __attribute__((overloadable)) NSStringFromNumber(const float val)
{
    return [NSString stringWithFormat:@"%f", val];
}

NSString * __attribute__((overloadable)) NSStringFromNumber(const double val)
{
    return [NSString stringWithFormat:@"%f", val];
}

NSString * __attribute__((overloadable)) NSStringFromNumber(const int8_t val)
{
    return [NSString stringWithFormat:@"%d", val];
}

NSString * __attribute__((overloadable)) NSStringFromNumber(const int16_t val)
{
    return [NSString stringWithFormat:@"%d", val];
}

NSString * __attribute__((overloadable)) NSStringFromNumber(const int32_t val)
{
    return [NSString stringWithFormat:@"%d", val];
}

NSString * __attribute__((overloadable)) NSStringFromNumber(const int64_t val)
{
    return [NSString stringWithFormat:@"%lld", val];
}

NSString * __attribute__((overloadable)) NSStringFromNumber(const u_int8_t val)
{
    return [NSString stringWithFormat:@"%d", val];
}

NSString * __attribute__((overloadable)) NSStringFromNumber(const u_int16_t val)
{
    return [NSString stringWithFormat:@"%d", val];
}

NSString * __attribute__((overloadable)) NSStringFromNumber(const u_int32_t val)
{
    return [NSString stringWithFormat:@"%d", val];
}

NSString * __attribute__((overloadable)) NSStringFromNumber(const u_int64_t val)
{
    return [NSString stringWithFormat:@"%llu", val];
}



