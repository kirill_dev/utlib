//
//  NSSet+UTMethods.h
//  UTLib
//
//  Created by Maksym Kareta on 3/15/15.
//  Copyright (c) 2015 Maksym Kareta. All rights reserved.
//

@interface NSSet(UTMethods)

- (NSArray *)sortedArrayUsingDescriptor:(NSSortDescriptor *)desciptor;
- (NSArray *)sortedArrayUsingKey:(NSString *)key ascending:(BOOL)ascending;
- (NSArray *)sortedArrayUsingKey:(NSString *)key;

@end
