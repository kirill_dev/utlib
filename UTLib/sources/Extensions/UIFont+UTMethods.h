//
//  UIFont+UTMethods.h
//  UTLib
//
//  Created by Maksym Kareta on 2/7/15.
//  Copyright (c) 2015 Maksym Kareta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont(UTMethods)

+ (void)applyFontWithFontName:(NSString *)name forAllLabelsInView:(UIView *)mainView includeSubviews:(BOOL)includeSubviews;
+ (void)changeFontForObject:(id)object withFontName:(NSString *)fontName;

@end
