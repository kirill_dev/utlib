//
//  UIImage+UTMethods.h
//  UTLib
//
//  Created by Maksym Kareta on 3/28/15.
//  Copyright (c) 2015 Maksym Kareta. All rights reserved.
//

@interface UIImage(UTMethods)

+ (UIImage *)imageByMergingImage:(UIImage *)image1 withImage:(UIImage *)image2;
- (UIImage *)mergeWithImage:(UIImage *)image2;
- (UIImage *)tintWithColor:(UIColor *)tintColor;
+ (UIImage *)clearImageWithSize:(CGSize)size;

@end
