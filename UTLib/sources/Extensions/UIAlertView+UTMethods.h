//
//  UIAlertView+PresentError.h
//  SportStream
//
//  Created by Maksym Kareta on 8/3/12.
//  Copyright (c) 2012 Maksym Kareta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertView (UTMethods)

/// Shows alert view with information from error.
///
/// @discussion Create and present UIAlertView, use localizedFailureReason as title and localizedDescription as message.
/// @param error Error to present.
/// @param okButtonTitle OK Button title.
+ (void)presentError:(NSError *)error okButtonTitle:(NSString *)okButtonTitle;

/// Shows alert view with information from error.
///
/// @discussion Create and present UIAlertView, use localizedFailureReason as title and localizedDescription as message. Use "OK" as ok button title.
/// @param error Error to present.
+ (void)presentError:(NSError *)error;

/// Shows alert witch conaint text.
///
/// @discussion Create and present UIAlertView, use nil as title and "text" as message.
/// @param text Text to present.
+ (void)showText:(NSString *)text;

@end
