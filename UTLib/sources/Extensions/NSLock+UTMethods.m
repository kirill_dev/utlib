//
//  NSLock_UTMethods.h
//  UTLib
//
//  Created by Maksym Kareta on 9/20/12.
//  Copyright (c) 2012 Maksym Kareta. All rights reserved.
//

#import "NSLock+UTMethods.h"

@implementation NSLock (UTMethods)

+ (void)lockBlock:(UTEmptyBlock)block withLocker:(id <NSLocking>)locker
{
    [locker lock];
    block();
    [locker unlock];
}

+ (void)lockBlock:(UTEmptyBlock)block withSemaphor:(dispatch_semaphore_t)sema
{
    dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    block();
    dispatch_semaphore_signal(sema);
}

@end
