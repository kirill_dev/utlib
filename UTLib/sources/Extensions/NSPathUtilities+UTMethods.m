//
//  NSPathUtilities+UTMethods.m
//  UTLib
//
//  Created by Maksym Kareta on 7/21/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

#import "NSPathUtilities+UTMethods.h"

NSString *UTDocumentsPathWithComponent(NSString *component)
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentDirPath = ([paths count] > 0) ? [paths lastObject] : nil;
    if (nil == documentDirPath)
    {
        return nil;
    }
    return [documentDirPath stringByAppendingPathComponent:component];
}
