//
//  NSDateFormatter+UTMethods.m
//  UTLib
//
//  Created by Maksym Kareta on 8/22/13.
//  Copyright (c) 2013 Maksym Kareta. All rights reserved.
//

#import "NSDateFormatter+UTMethods.h"

@implementation NSDateFormatter(UTMethods)

static NSMutableDictionary *cache = nil;
static dispatch_semaphore_t semaphor = NULL;

+ (void)prepareCache
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        cache = [NSMutableDictionary dictionary];
        semaphor = dispatch_semaphore_create(1);
    });
}

+ (void)registerFormatter:(NSDateFormatter *)dateFormatter withID:(NSString *)ID
{
    [self prepareCache];
    dispatch_semaphore_wait(semaphor, DISPATCH_TIME_FOREVER);
    [cache setObject:dateFormatter forKey:ID];
    dispatch_semaphore_signal(semaphor);
}

+ (NSDateFormatter *)dateFromatterWithID:(NSString *)ID
{
    [self prepareCache];
    NSDateFormatter *result = nil;
    dispatch_semaphore_wait(semaphor, DISPATCH_TIME_FOREVER);
    result = [cache objectForKey:ID];
    dispatch_semaphore_signal(semaphor);
    return result;
}

+ (void)purgeCaches
{
    [self prepareCache];
    dispatch_semaphore_wait(semaphor, DISPATCH_TIME_FOREVER);
    [cache removeAllObjects];
    dispatch_semaphore_signal(semaphor);
}

@end
