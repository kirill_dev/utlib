//
//  NSMutableDictionary+UTMethods.h
//  UTLib
//
//  Created by Maksym Kareta on 5/16/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

@interface NSMutableDictionary(UTMethods)

- (void)safeSetObject:(id)anObject forKey:(id<NSCopying>)aKey;

@end
