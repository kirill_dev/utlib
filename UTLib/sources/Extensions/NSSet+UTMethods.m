//
//  NSSet+UTMethods.m
//  UTLib
//
//  Created by Maksym Kareta on 3/15/15.
//  Copyright (c) 2015 Maksym Kareta. All rights reserved.
//

#import "NSSet+UTMethods.h"

@implementation NSSet(UTMethods)

- (NSArray *)sortedArrayUsingDescriptor:(NSSortDescriptor *)desciptor
{
    return [self sortedArrayUsingDescriptors:@[desciptor]];
}

- (NSArray *)sortedArrayUsingKey:(NSString *)key ascending:(BOOL)ascending
{
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:key ascending:ascending];
    return [self sortedArrayUsingDescriptors:@[descriptor]];
}

- (NSArray *)sortedArrayUsingKey:(NSString *)key
{
    return [self sortedArrayUsingKey:key ascending:YES];
}


@end
