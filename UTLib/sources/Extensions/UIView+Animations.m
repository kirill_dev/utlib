//
//  UIView+animations.m
//  UTLib
//
//  Created by Maksym Kareta on 4/29/12.
//  Copyright (c) 2012 Maksym Kareta, Ltd. All rights reserved.
//

#import "UIView+Animations.h"
#import "UTAnimationSequence.h"

#if TARGET_IPHONE_SIMULATOR
#import <objc/objc-runtime.h>
#else
#import <objc/message.h>
#endif

static const char kUIViewUTAnimationsSequenceKey;

@implementation UIView (UTAnimations)

- (void)prepareForAnimationSequence
{
    UTAnimationSequence *animations = [UTAnimationSequence new];
	objc_setAssociatedObject(self, &kUIViewUTAnimationsSequenceKey, animations, OBJC_ASSOCIATION_RETAIN);
}

- (void)addAnimationBlock:(UIViewUTAnimationsSequenceBlock)block completion:(UIViewUTAnimationsSequenceCompletionBlock)completion duration:(NSTimeInterval)duration
{
	UTAnimationSequence *animations = objc_getAssociatedObject(self, &kUIViewUTAnimationsSequenceKey);
    [animations addAnimationWithDuration:duration block:block completion:completion];
}

- (void)addAnimationBlock:(UIViewUTAnimationsSequenceBlock)block duration:(NSTimeInterval)duration
{
    UTAnimationSequence *animations = objc_getAssociatedObject(self, &kUIViewUTAnimationsSequenceKey);
    [animations addAnimationWithDuration:duration block:block];
}

- (void)commitAnimationSequence
{
    UTAnimationSequence *animations = objc_getAssociatedObject(self, &kUIViewUTAnimationsSequenceKey);
    [animations commitAnimationSequence];
}

- (void)commitAnimationSequenceWithCompletionBlock:(UIViewUTAnimationsSequenceCompletionBlock)block
{
    UTAnimationSequence *animations = objc_getAssociatedObject(self, &kUIViewUTAnimationsSequenceKey);
    [animations commitAnimationSequenceWithCompletionBlock:block];
}

@end
