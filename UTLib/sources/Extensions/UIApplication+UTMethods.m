//
//  UIApplication+UTMethods.m
//  UTLib
//
//  Created by Maksym Kareta on 1/2/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

#import "UIApplication+UTMethods.h"

UIApplication *UTSharedApplication(void)
{
    return [UIApplication sharedApplication];
}


@implementation UIApplication(UTMethods)

+ (void)showDirectionsWithAppleMapsFrom:(CLLocationCoordinate2D)start to:(CLLocationCoordinate2D)stop
{
    NSString *string = [NSString stringWithFormat:@"http://maps.apple.com/?daddr=%lf,%lf&saddr=%lf,%lf", stop.latitude, stop.longitude, start.latitude, start.longitude];
    NSURL *url = [NSURL URLWithString:string];
    [UTSharedApplication() openURL:url];
}

@end
