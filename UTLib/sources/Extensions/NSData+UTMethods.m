//
//  NSData+UTMethods.m
//  UTLib
//
//  Created by Max Kareta on 4/30/15.
//  Copyright (c) 2015 Max Kareta. All rights reserved.
//

#import "NSData+UTMethods.h"
#import "UTRandom.h"

@implementation NSData(UTMethods)

+ (NSData *)randomDataWithLength:(NSUInteger)length
{
    NSMutableData *data = [NSMutableData dataWithCapacity:length];
    while (length >= 4)
    {
        u_int32_t value = UTRandom32();
        [data appendBytes:&value length:4];
        length -= 4;
    }
    while (length >= 1)
    {
        u_int8_t value = UTRandom8();
        [data appendBytes:&value length:1];
        length--;
    }
    return data;
}

@end
