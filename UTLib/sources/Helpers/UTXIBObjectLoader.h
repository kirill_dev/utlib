//
//  UTXIBObjectLoader.h
//  UTLib
//
//  Created by Maksym Kareta on 11/13/13.
//  Copyright (c) 2013 Maksym Kareta. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UTXIBObjectLoader : NSObject

@property (strong, nonatomic) IBOutlet id object;

///Require object outlet
+ (id)loadXIBNamed:(NSString *)name;
+ (id)loadTopViewFromXib:(NSString *)name;

@end
