//
//  UTDelegateArray.m
//  UTLib
//
//  Created by Maksym Kareta on 9/3/13.
//  Copyright (c) 2013 Maksym Kareta. All rights reserved.
//

#import "UTDelegateArray.h"

@interface UTDelegateArray()

@property (strong, nonatomic) NSMutableArray *delegateArray;
@property (assign, nonatomic) NSObject *object;

@end

@implementation UTDelegateArray

- (id)initWithObj:(NSObject *)object
{
    NSAssert(object != nil, @"Root object of Delegate array can't be nil");
    self = [super init];
    if (nil != self)
    {
        self.object = object;
        self.delegateArray = (NSMutableArray *)CFBridgingRelease(CFArrayCreateMutable(NULL, 0, NULL));
    }
    return self;
}

- (void)delegatePermormSelector:(SEL)selector
{
    for (id obj in self.delegateArray)
    {
        if ([obj respondsToSelector:selector])
        {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
            [obj performSelector:selector withObject:self.object];
#pragma clang diagnostic pop
        }
    };
}

- (void)delegatePermormSelector:(SEL)selector withObject:(id)objToSend
{
    for (id obj in self.delegateArray)
    {
        if ([obj respondsToSelector:selector])
        {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
            [obj performSelector:selector withObject:self.object withObject:objToSend];
#pragma clang diagnostic pop
        }
    };
}

- (void)addDelegate:(NSObject *)delegate
{
    [self.delegateArray addObject:delegate];
}

- (void)removeDelegate:(NSObject *)delegate
{
    [self.delegateArray removeObject:delegate];
}

- (void)removeAllObjects
{
    [self.delegateArray removeAllObjects];
}

- (void)enumerateObjectsUsingBlock:(UTBlockWithObject)block
{
    NSAssert(block != NULL, @"Block can't be NULL");
    for (id obj in self.delegateArray)
    {
        block(obj);
    }
}

@end
