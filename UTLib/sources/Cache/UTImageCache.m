//
//  UTImageCache.m
//  UsefulThings
//
//  Created by Maksym Kareta on 3/8/12.
//  Copyright (c) 2012 Maksym Kareta. All rights reserved.
//

#import "UTImageCache.h"
#import "NSString+UTMethods.h"

@interface UTImageCache()

@property (strong, nonatomic) NSCache *cache;

@end

@implementation UTImageCache
@synthesize cache = _cache;

#pragma mark - singleton stuff

static UTImageCache *_instance;

+ (UTImageCache *)sharedInstance
{
	static dispatch_once_t predicate;
	void (^createInstanceBlock)(void) = ^(void)
	{
		_instance = [[self alloc] init];
	};
	dispatch_once(&predicate, createInstanceBlock);
	return _instance;
}

- (id)init
{
	self = [super init];
	if (nil != self)
	{
		self.cache = [NSCache new];
	}
	return self;
}

#pragma mark -

- (NSString *)addUIImage:(UIImage *)image
{
    if (nil == image)
    {
        return nil;
    }
    NSString *key = [NSString UUID];
    [self addUIImage:image withKey:key];
    return key;
}

- (void)removeImageForKey:(NSString *)key
{
    [self.cache removeObjectForKey:key];
}

- (void)addUIImage:(UIImage *)image withKey:(NSString *)key
{
	if (nil == image || nil == key)
	{
		return;
	}
	NSUInteger size = (NSUInteger)([image size].width * [image size].height * 4);
	[self.cache setObject:image forKey:key cost:size];
}

- (UIImage *)UIImageForKey:(NSString *)key
{
	return [self.cache objectForKey:key];
}

- (void)setMaximumSize:(NSUInteger)size
{
	[self.cache setTotalCostLimit:size * 1024 * 1024];
}

- (void)clearCache
{
	[self.cache removeAllObjects];
}

- (id)objectForKeyedSubscript:(NSString *)key;
{
    return [self.cache objectForKey:key];
}

- (void)setObject:(UIImage *)obj forKeyedSubscript:(NSString *)key
{
    [self addUIImage:obj withKey:key];
}

@end
