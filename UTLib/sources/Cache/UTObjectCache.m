//
//  UTObjectCache.m
//  UTLib
//
//  Created by Maksym Kareta on 4/4/12.
//  Copyright (c) 2012 Maksym Kareta. All rights reserved.
//

#import "UTObjectCache.h"

@interface UTObjectCache()

@property (strong, nonatomic) id cache;

@end


@implementation UTObjectCache

- (id)initWithCacheType:(UTObjectCacheType)type
{
	self = [super init];
    if (nil != self) 
    {
		if (kUTObjectCacheTypeNSCache == type)
		{
			self.cache = [[NSCache alloc] init];
		}
		else
		{
			self.cache = [NSMutableDictionary dictionary];
		}
    }
    return self;
}

- (id)init 
{
	self = [self initWithCacheType:kUTObjectCacheTypeNSCache];
    return self;
}

- (void)clear
{
	[self.cache removeAllObjects];
}

- (id)objectForKey:(NSString *)name
{
	return (self.cache)[name];
}

- (void)setObject:(id)object withName:(NSString *)name
{
	UTAssert(nil != object, @"try insert nil object to cache");
	(self.cache)[name] = object;
}

- (void)setObject:(id)object forKey:(NSString *)name cost:(NSUInteger)cost
{
	if ([self.cache isKindOfClass:[NSCache class]])
	{
		[self.cache setObject:object forKey:name cost:cost];
	}
	else 
	{
		(self.cache)[name] = object;
	}
}

@end
