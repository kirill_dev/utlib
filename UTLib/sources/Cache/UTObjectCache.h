//
//  UTObjectCache.h
//  UTLib
//
//  Created by Maksym Kareta on 4/4/12.
//  Copyright (c) 2012 Maksym Kareta. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum 
{
	kUTObjectCacheTypeNSCache = 0,
	kUTObjectCacheTypeDictionary
} UTObjectCacheType;

@interface UTObjectCache : NSObject

//return NSCache or NSDictionary
@property (strong, nonatomic, readonly) id cache;

- (id)initWithCacheType:(UTObjectCacheType)type;
- (id)objectForKey:(NSString *)name;
- (void)setObject:(id)object withName:(NSString *)name;
/*
 NOTE: if cache type is dictionary, then cost = 0
 */
- (void)setObject:(id)object forKey:(NSString *)name cost:(NSUInteger)cost;
- (void)clear;

@end
