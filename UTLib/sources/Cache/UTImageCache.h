//
//  UTImageCache.h
//  UsefulThings
//
//  Created by Maksym Kareta on 3/8/12.
//  Copyright (c) 2012 Maksym Kareta. All rights reserved.
//

@import UIKit;

///It's safe to create few instances of UTImageCache
/// but try to avoid doing this.
/// Recommended to use for all images
///@note class based on NSCache
///@note for key you can use NSString+UUID
@interface UTImageCache : NSObject

+ (UTImageCache *)sharedInstance;

- (NSString *)addUIImage:(UIImage *)image;

///@note object with same key will be replaced, to avoid this use UUID keys
- (void)addUIImage:(UIImage *)image withKey:(NSString *)key;
- (UIImage *)UIImageForKey:(NSString *)key;
- (void)removeImageForKey:(NSString *)key;

///@note limits are imprecise/not strict
- (void)setMaximumSize:(NSUInteger)size;//in MB

- (void)clearCache;

- (id)objectForKeyedSubscript:(NSString *)key;
- (void)setObject:(UIImage *)obj forKeyedSubscript:(NSString *)key;

@end
