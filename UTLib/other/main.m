//
//  main.m
//  UTLib
//
//  Created by Maksym Kareta on 9/9/12.
//  Copyright (c) 2012 Maksym Kareta. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
