//
//  UTTableViewController.m
//  UTTable
//
//  Created by Maksym Kareta on 5/12/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

#import "UTTableViewController.h"
#import "UTCellInfo.h"
#import "UITableViewCell+CellCreation.h"
#import "UTSectionInfo.h"

@interface UTTableViewController ()

@property (strong, nonatomic) NSArray *sections;

@end

@implementation UTTableViewController

- (void)pushWithNavigationController:(UINavigationController *)nc
{
    [nc pushViewController:self animated:YES];
}

- (void)rebuildItems
{
    if (self.isViewLoaded && nil != self.view.window)
    {
        [self.tableView beginUpdates];
        [self.tableView deleteSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, self.sections.count)] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self buildTable];
        [self.tableView insertSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, self.sections.count)] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tableView endUpdates];
    }
}

- (void)reloadItems
{
    [self.tableView reloadSections:[[NSIndexSet alloc] initWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (void)buildTable
{
    [self cleanItems];
    if ([self respondsToSelector:@selector(buildSections)])
    {
        self.sections = [self buildSections];
    }
    else if ([self respondsToSelector:@selector(buildItems)])
    {
        UTSectionInfo *info = [UTSectionInfo sectionWithItems:[self buildItems]];
        self.sections = @[info];
    }
    [self.sections enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [obj setIndex:idx];
        [obj setTableView:self.tableView];
    }];
}

- (void)cleanItems
{
    for (UTSectionInfo *info in self.sections)
    {
        info.tableView = nil;
        for (UTCellInfo *cell in info.items)
        {
            cell.tableView = nil;
            cell.indexPath = nil;
        }
    }
    self.sections = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self buildTable];
    [self.tableView reloadData];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [self cleanItems];
    [super viewDidDisappear:animated];
}

- (UTCellInfo *)cellForIndexPath:(NSIndexPath *)path
{
    UTSectionInfo *section = self.sections[path.section];
    return section.items[path.row];
}


#pragma mark - Table view data source

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return [self.sections[section] footerTitle];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [self.sections[section] headerTitle];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.sections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.sections[section] items].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UTCellInfo *cellInfo = [self cellForIndexPath:indexPath];
    cellInfo.indexPath = indexPath;
    cellInfo.tableView = tableView;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellInfo.identifier];
    if ([cell.accessoryView isKindOfClass:[UIControl class]])
    {
        [(UIControl *)cell.accessoryView removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
    }
    if (nil == cell)
    {
        cell = [UITableViewCell cellWithInfo:cellInfo];
    }
    else
    {
        [cell updateWithInfo:cellInfo];
    }
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UTCellInfo *info = [self cellForIndexPath:indexPath];
    [info performAction];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
