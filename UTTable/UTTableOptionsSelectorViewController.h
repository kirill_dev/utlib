//
//  UTTableOptionsSelectorViewController.h
//  UTTable
//
//  Created by Maksym Kareta on 5/12/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UTTableOptionsSelectorViewController;

@protocol UTOptionsSelectorDelegate <NSObject>

///@param options Selected option with sub options if up-down (option->sub option->sub option) order
- (void)optionsSelector:(UTTableOptionsSelectorViewController *)vc didSelectOptions:(NSArray *)options;

@end


@interface UTTableOptionsSelectorViewController : UITableViewController <UTOptionsSelectorDelegate>

@property (weak, nonatomic) id <UTOptionsSelectorDelegate> delegate;

///Array of options. Option is NSDicitionary that contain 2 keys/objects: text - string, value = object/"sub options dicionary"
- (id)initWithOptions:(NSArray *)options;

- (void)didSelectOptions:(NSArray *)options;

@end

