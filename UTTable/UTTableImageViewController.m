//
//  UTTableImageViewController.m
//  UTTable
//
//  Created by Maksym Kareta on 5/12/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

#import "UTTableImageViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface UTTableImageViewController ()

@property (strong, nonatomic) NSObject *object;
@property (copy, nonatomic) NSString *keypath;
@property (strong, nonatomic) UIImageView *imageView;

@end

@implementation UTTableImageViewController

- (void)setObject:(NSObject *)object andKeyPath:(NSString *)keypath
{
    self.object = object;
    self.keypath = keypath;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor grayColor];
    self.imageView = [UIImageView new];
    self.imageView.frame = self.view.bounds;
    self.imageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:self.imageView];
    self.imageView.contentMode = UIViewContentModeCenter;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    CFAbsoluteTime time = CFAbsoluteTimeGetCurrent();
    UIImage *image = [self.object valueForKeyPath:self.keypath];
    time = CFAbsoluteTimeGetCurrent() - time;
    self.title = [NSString stringWithFormat:@"Loading time: %lf", time];
    self.imageView.image = image;
}

@end
