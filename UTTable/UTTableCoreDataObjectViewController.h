//
//  UTTableCoreDataObjectViewController.h
//  UTTable
//
//  Created by Maksym Kareta on 5/12/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

#import "UTTableViewController.h"

@import CoreData;

@interface UTTableCoreDataObjectViewController : UTTableViewController

///If storage queue is NULL, main queue will be used 
@property (strong, nonatomic) dispatch_queue_t storageQueue;
@property (strong, nonatomic) NSManagedObject *object;

@end

@protocol UTCoreDataObjectActions <NSObject>

- (NSArray *)UTTableCoreDataObjectActions;
- (NSArray *)UTTableCoreDataObjectActionsWithNavigationController:(UINavigationController *)nc;

@end
