//
//  UTTableViewController.h
//  UTTable
//
//  Created by Maksym Kareta on 5/12/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

@class UTCellInfo;

@protocol UTTableViewControllerWithSections <NSObject>
@optional
///methods to overwrite
- (NSArray *)buildSections;
- (NSArray *)buildItems;

@end

@interface UTTableViewController : UITableViewController <UTTableViewControllerWithSections>

- (void)pushWithNavigationController:(UINavigationController *)nc;
- (void)rebuildItems;

- (void)reloadItems;

///methods to overwrite
- (void)cleanItems;

@end

