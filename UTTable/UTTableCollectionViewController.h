//
//  UTTableCollectionViewController.h
//  UTTable
//
//  Created by Maksym Kareta on 5/12/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

#import "UTTableViewController.h"

@class UTCellInfo;

@interface UTTableCollectionViewController : UTTableViewController

- (UTCellInfo *)cellInfoForObject:(id)object withName:(NSString *)name;

@end
