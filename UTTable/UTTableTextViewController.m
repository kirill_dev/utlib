//
//  UTTableTextViewController.m
//  UTTable
//
//  Created by Maksym Kareta on 5/12/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

#import "UTTableTextViewController.h"

@interface UTTableTextViewController ()

@property (strong, nonatomic) UITextView *textView;

@end

@implementation UTTableTextViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.textView = [[UITextView alloc] initWithFrame:self.view.bounds];
    self.textView.editable = NO;
    [self.textView setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth];
    [self.view setAutoresizesSubviews:YES];
    [self.view addSubview:self.textView];
    self.textView.text = self.text;
}

- (void)setText:(NSString *)text
{
    _text = text;
    self.textView.text = text;
    [self.textView flashScrollIndicators];
}

- (void)setFontSize:(CGFloat)size
{
    [self.textView setFont:[UIFont systemFontOfSize:size]];
}

@end
