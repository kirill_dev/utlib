//
//  UTTableFileViewController.m
//  UTTable
//
//  Created by Maksym Kareta on 5/12/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

#import "UTTableFileViewController.h"
#import "UTTableTextViewController.h"

@interface UTTableFileViewController ()

@property (strong, nonatomic) NSFileHandle *fileHandle;
@property (assign, nonatomic) BOOL readInitiated;

@end

@implementation UTTableFileViewController

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSFileHandle *fh = [NSFileHandle fileHandleForReadingAtPath:self.filePath];
    if (nil == fh)
    {
        [self setText:@"Can't open file"];
    }
    else
    {
        self.fileHandle = fh;
        NSData *data = [fh readDataToEndOfFile];
        [self setText:[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding]];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData:) name:NSFileHandleDataAvailableNotification object:fh];
        [fh waitForDataInBackgroundAndNotify];
    }
    [self setFontSize:10];
}

- (void)updateData:(NSNotification *)notification
{
    if (!self.readInitiated)
    {
        self.readInitiated = YES;
        double delayInSeconds = 2.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            self.readInitiated = NO;
            NSData *data = [self.fileHandle readDataToEndOfFile];
            NSString *string = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            [self setText:[[self text] stringByAppendingString:string]];
        });
    }
    [self.fileHandle waitForDataInBackgroundAndNotify];
}

@end
