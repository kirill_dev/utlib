//
//  UTTableCoreDataObjectViewControlle.m
//  UTTable
//
//  Created by Maksym Kareta on 5/12/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

#import "UTTableCoreDataObjectViewController.h"
#import "UTTableArrayViewController.h"
#import "UTCellInfo.h"
#import "UTSectionInfo.h"

@interface UTTableCoreDataObjectViewController ()

@property (strong, nonatomic) NSArray *cachedItems;

@end

@implementation UTTableCoreDataObjectViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(reloadCoraDataTable:)];
    [self.navigationItem setRightBarButtonItem:item];
}

- (void)reloadCoraDataTable:(id)sender
{
    self.cachedItems = nil;
    [self rebuildItems];
}

- (NSArray *)buildSections
{
    if (nil == self.object)
    {
        return @[[UTSectionInfo sectionWithItems:@[[UTCellInfo cellWithText:@"Object is nil"]]]];
    }
    else if (nil != self.cachedItems)
    {
        return self.cachedItems;
    }
    else
    {
        __block NSMutableArray* items = [NSMutableArray array];
      
        UTCellActionBlock openArrayBlock = ^(UTCellInfo* info) {
            UTTableArrayViewController *vc = [UTTableArrayViewController new];
            [vc setArray:[info.userInfo allObjects]];
            [vc pushWithNavigationController:self.navigationController];
        };
        
        UTCellActionBlock openObjectBlock = ^(UTCellInfo* info) {
            UTTableCoreDataObjectViewController *vc = [UTTableCoreDataObjectViewController new];
            [vc setObject:info.userInfo];
            [vc pushWithNavigationController:self.navigationController];
        };

        dispatch_queue_t queue = self.storageQueue;
        if (queue == NULL)
        {
            queue = dispatch_get_main_queue();
        }
        
        dispatch_async(queue, ^{
            NSDictionary *attributes = [[self.object entity] attributesByName];
            NSDictionary *relationships = [[self.object entity] relationshipsByName];
            [attributes enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                NSString *val = [[self.object valueForKey:key] description];
                UTCellInfo *info = [UTCellInfo textViewerCellWithText:key text2:val navigationController:self.navigationController];
                [items addObject:info];
            }];
            [relationships enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                UTCellInfo *info = nil;
                if ([obj isToMany])
                {
                    info = [UTCellInfo cellWithText:key type:kUTCellTypeChoose actionBlock:openArrayBlock];
                }
                else
                {
                    info = [UTCellInfo cellWithText:key type:kUTCellTypeChoose actionBlock:openObjectBlock];
                }
                [info setUserInfo:[self.object valueForKey:key]];
                [items addObject:info];
            }];
            dispatch_async(dispatch_get_main_queue(), ^{
                NSArray *actions = nil;
                if ([self.object respondsToSelector:@selector(UTTableCoreDataObjectActions)])
                {
                    id obj = self.object;
                    actions = [obj UTTableCoreDataObjectActions];
                }
                else if ([self.object respondsToSelector:@selector(UTTableCoreDataObjectActionsWithNavigationController:)])
                {
                    id obj = self.object;
                    actions = [obj UTTableCoreDataObjectActionsWithNavigationController:self.navigationController];
                }
                UTSectionInfo *mainSections = [UTSectionInfo sectionWithItems:items];
                UTSectionInfo *actionsSection = [UTSectionInfo sectionWithItems:actions headerTitle:@"Actions"];
                self.cachedItems = @[mainSections, actionsSection];
                [self rebuildItems];
            });
        });
        return @[[UTSectionInfo sectionWithItems:@[[UTCellInfo cellWithText:@"Loading..."]]]];
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    self.cachedItems = nil;
}

@end
