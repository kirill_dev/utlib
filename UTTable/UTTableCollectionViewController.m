//
//  UTTableCollectionViewController.m
//  UTTable
//
//  Created by Maksym Kareta on 5/12/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

#import "UTTableCollectionViewController.h"
#import "UTTableArrayViewController.h"
#import "UTTableDictionaryViewController.h"
#import "UTCellInfo.h"
#import "UTTableCoreDataObjectViewController.h"

@import CoreData;

@interface UTTableCollectionViewController ()

@end

@implementation UTTableCollectionViewController

- (UTCellInfo *)cellInfoForObject:(id)obj withName:(NSString *)name
{
    UTCellActionBlock viewDictBlock = ^(UTCellInfo *info){
        UTTableDictionaryViewController *vc = [UTTableDictionaryViewController new];
        [self.navigationController pushViewController:vc animated:YES];
        [vc setDicionary:info.userInfo];
    };
    
    UTCellActionBlock viewArrayBlock = ^(UTCellInfo *info){
        UTTableArrayViewController *vc = [UTTableArrayViewController new];
        [self.navigationController pushViewController:vc animated:YES];
        [vc setArray:info.userInfo];
    };
    
    UTCellActionBlock viewCoreDataBlock = ^(UTCellInfo *info){
        UTTableCoreDataObjectViewController *vc = [UTTableCoreDataObjectViewController new];
        [vc setObject:info.userInfo];
        [self.navigationController pushViewController:vc animated:YES];
    };
    
    
    UTCellInfo *info = nil;
    if ([obj isKindOfClass:[NSDictionary class]])
    {
        info = [UTCellInfo cellWithText:name actionBlock:viewDictBlock];
    }
    else if ([obj isKindOfClass:[NSArray class]])
    {
        info = [UTCellInfo cellWithText:name actionBlock:viewArrayBlock];
    }
    else if ([obj isKindOfClass:[NSManagedObject class]])
    {
        info = [UTCellInfo cellWithText:name actionBlock:viewCoreDataBlock];
    }
    else
    {
        info = [UTCellInfo textViewerCellWithText:name text2:[obj description] navigationController:self.navigationController];
    }
    info.userInfo = obj;
    return info;

}

@end
