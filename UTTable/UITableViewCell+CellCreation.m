//
//  UITableViewCell+CellCreation.m
//  UTTable
//
//  Created by Maksym Kareta on 5/12/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

#import "UITableViewCell+CellCreation.h"
#import "UTCellInfo.h"

static const CGFloat kDefaultFontSize = 12.0;

@implementation UITableViewCell(CellCreation)

+ (UITableViewCell *)cellWithInfo:(UTCellInfo *)info
{
    UITableViewCell *cell = nil;
    cell = [[UITableViewCell alloc] initWithStyle:info.style reuseIdentifier:info.identifier];
    if (info.style == UITableViewCellStyleSubtitle)
    {
        cell.detailTextLabel.textColor = [UIColor grayColor];
    }
    [cell updateWithInfo:info];
    return cell;
}

- (void)updateWithInfo:(UTCellInfo *)info
{
    if (!info.canPerformAction)
    {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    self.textLabel.text = info.text;
    self.detailTextLabel.text = info.text2;
    self.detailTextLabel.font = [UIFont systemFontOfSize:kDefaultFontSize];
    self.textLabel.font = [UIFont systemFontOfSize:kDefaultFontSize];
    self.accessoryView = nil;
    self.imageView.image = info.image;
    if (info.showActivityIndicator)
    {
        UIActivityIndicatorView *view = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [view startAnimating];
        self.accessoryView = view;
    }
    else
    {
        switch (info.type) {
            case kUTCellTypeChoose:
            {
                self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                break;
            }
            case kUTCellTypeSwitch:
            {
                UISwitch *sw = [[UISwitch alloc] init];
                self.accessoryView = sw;
                sw.on = info.on;
    #pragma clang diagnostic push
    #pragma clang diagnostic ignored "-Wselector"
    #pragma clang diagnostic ignored "-Wundeclared-selector"
                [sw addTarget:info action:@selector(changeState:) forControlEvents:UIControlEventValueChanged];
    #pragma clang diagnostic pop 
                break;
            }
            case kUTCellTypeText:
            {
                self.accessoryType = UITableViewCellAccessoryNone;
                break;
            }
            default:
                break;
        }
    }
}

@end
