//
//  UTCellInfo.h
//  UTTable
//
//  Created by Maksym Kareta on 5/12/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, UTCellType)
{
    kUTCellTypeText = 0,
    kUTCellTypeChoose,
    kUTCellTypeSwitch,
};

@class UTCellInfo;

typedef void (^UTCellActionBlock)(UTCellInfo* info);
@interface UTCellInfo : NSObject

@property (copy, nonatomic, readonly) UTCellActionBlock block;
@property (strong, nonatomic, readonly) NSString *identifier;
@property (assign, nonatomic) UTCellType type;
@property (strong, nonatomic) UIImage *image;
@property (strong, nonatomic) NSString *text;
@property (strong, nonatomic) NSString *text2;
@property (assign, nonatomic, getter = isOn) BOOL on;
@property (strong, nonatomic) id userInfo;
@property (weak, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSIndexPath *indexPath;
@property (assign, nonatomic) UITableViewCellStyle style;
@property (assign, nonatomic) BOOL showActivityIndicator;

+ (void)setDefaultStyle:(UITableViewCellStyle)style;

+ (UTCellInfo *)cellWithType:(UTCellType)type actionBlock:(UTCellActionBlock)block;
+ (UTCellInfo *)cellWithText:(NSString *)text text2:(NSString *)text2;
+ (UTCellInfo *)cellWithText:(NSString *)text text2:(NSString *)text2 actionBlock:(UTCellActionBlock)block;
+ (UTCellInfo *)cellWithText:(NSString *)text text2:(NSString *)text2 type:(UTCellType)type;
+ (UTCellInfo *)cellWithText:(NSString *)text text2:(NSString *)text2 type:(UTCellType)type actionBlock:(UTCellActionBlock)block;
+ (UTCellInfo *)cellWithText:(NSString *)text;
+ (UTCellInfo *)cellWithFormat:(NSString *)text, ...;
+ (UTCellInfo *)cellWithText:(NSString *)text type:(UTCellType)type;
+ (UTCellInfo *)cellWithText:(NSString *)text type:(UTCellType)type actionBlock:(UTCellActionBlock)block;
+ (UTCellInfo *)cellWithText:(NSString *)text actionBlock:(UTCellActionBlock)block;
+ (UTCellInfo *)cellWithText:(NSString *)text userInfo:(id)userInfo actionBlock:(UTCellActionBlock)block;

- (BOOL)canPerformAction;
- (void)performAction;
- (void)reload;

@end

@interface UTCellInfo(NavigationCells)

+ (UTCellInfo *)imageViewerCellWithText:(NSString *)text navigationController:(UINavigationController *)nc object:(NSObject *)object keyPath:(NSString *)keyPath;
+ (UTCellInfo *)textViewerCellWithText:(NSString *)text text2:(NSString *)text2 navigationController:(UINavigationController *)nc;

@end

