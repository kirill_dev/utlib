//
//  UTTableArrayViewController.m
//  UTTable
//
//  Created by Maksym Kareta on 5/12/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

#import "UTTableArrayViewController.h"
#import "UTTableDictionaryViewController.h"

@interface UTTableArrayViewController ()

@property (strong, nonatomic) NSArray *dataArray;

@end

@implementation UTTableArrayViewController

- (NSArray *)buildItems
{
    NSMutableArray *items = [NSMutableArray arrayWithCapacity:self.dataArray.count];
    [self.dataArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSString *name = [self nameForRecord:obj atIndex:idx];
        [items addObject:[self cellInfoForObject:obj withName:name]];
    }];
    return items;
}

- (void)setArray:(NSArray *)array
{
    self.dataArray = array;
    if (self.isViewLoaded)
    {
        [self buildItems];
        [self.tableView reloadData];
    }
}

- (NSString *)nameForRecord:(NSObject *)record atIndex:(NSUInteger)index
{
    NSString *name = nil;
    
    if ([record respondsToSelector:@selector(UTTableRecordDescription)])
    {
        name = [NSString stringWithFormat:@"%llu. %@", (u_int64_t)index, [(id <UTTableRecord>)record UTTableRecordDescription]];
    }
    else
    {
        name = [NSString stringWithFormat:@"Record %llu", (u_int64_t)index];
    }
    return name;
}

@end
