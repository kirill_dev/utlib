//
//  UTTableDictionaryViewController.m
//  UTTable
//
//  Created by Maksym Kareta on 5/12/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

#import "UTTableDictionaryViewController.h"

@interface UTTableDictionaryViewController ()

@property (strong, nonatomic) NSDictionary *dataDict;

@end

@implementation UTTableDictionaryViewController

- (NSArray *)buildItems
{
    NSMutableArray *items = [NSMutableArray arrayWithCapacity:self.dataDict.count];
    
    [self.dataDict enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        NSString *name = [key isKindOfClass:[NSString class]] ? key : [key description];
        [items addObject:[self cellInfoForObject:obj withName:name]];
    }];
    return items;
}

- (void)setDicionary:(NSDictionary *)dict
{
    self.dataDict = dict;
    if (self.isViewLoaded)
    {
        [self buildItems];
        [self.tableView reloadData];
    }
}


@end
