//
//  UTSectionInfo.m
//  UTLib
//
//  Created by Maksym Kareta on 8/16/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

#import "UTSectionInfo.h"

@interface UTSectionInfo()

@property (strong, nonatomic) NSString *headerTitle;
@property (strong, nonatomic) NSString *footerTitle;
@property (strong, nonatomic) NSArray *items;

@end

@implementation UTSectionInfo

+ (instancetype)sectionWithItems:(NSArray *)items
{
    UTSectionInfo *info = [self new];
    info.items = items;
    return info;
}

+ (instancetype)sectionWithItems:(NSArray *)items headerTitle:(NSString *)title
{
    return [self sectionWithItems:items headerTitle:title footerTitle:nil];
}

+ (instancetype)sectionWithItems:(NSArray *)items footerTitle:(NSString *)title
{
    return [self sectionWithItems:items headerTitle:nil footerTitle:title];
}

+ (instancetype)sectionWithItems:(NSArray *)items headerTitle:(NSString *)header footerTitle:(NSString *)footer
{
    UTSectionInfo *info = [self sectionWithItems:items];
    info.headerTitle = header;
    info.footerTitle = footer;
    return info;
}

@end

@implementation UTMutableSectionInfo

@dynamic items;

- (void)setItems:(NSArray *)items
{
    [super setItems:[items mutableCopy]];
}

- (void)appendWithCellInfo:(UTCellInfo *)cellInfo
{
    [self.items addObject:cellInfo];
    NSArray *indexes = @[[NSIndexPath indexPathForRow:self.items.count - 1 inSection:self.index]];
    UITableView *tableView = self.tableView;
    [tableView insertRowsAtIndexPaths:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
}

@end
