//
//  UTCellInfo.m
//  UTTable
//
//  Created by Maksym Kareta on 5/12/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

#import "UTCellInfo.h"
#import "UTTableImageViewController.h"
#import "UTTableTextViewController.h"

@interface UTCellInfo()

@property (copy, nonatomic, readwrite) UTCellActionBlock block;
@property (strong, nonatomic, readwrite) NSString *identifier;

@end

@implementation UTCellInfo

static UITableViewCellStyle sDefaultStyle = UITableViewCellStyleSubtitle;

+ (void)setDefaultStyle:(UITableViewCellStyle)style
{
    sDefaultStyle = style;
}

+ (UTCellInfo *)cellWithType:(UTCellType)type actionBlock:(UTCellActionBlock)block
{
    UTCellInfo *res = [UTCellInfo new];
    res.type = type;
    res.block = block;
    res.style = sDefaultStyle;
    [res updateIdentifier];
    return res;
}

+ (UTCellInfo *)cellWithText:(NSString *)text text2:(NSString *)text2 type:(UTCellType)type actionBlock:(UTCellActionBlock)block
{
    UTCellInfo *cell = [self cellWithText:text text2:text2 actionBlock:block];
    cell.type = type;
    return cell;
}

+ (UTCellInfo *)cellWithText:(NSString *)text text2:(NSString *)text2
{
    UTCellInfo * cell = [UTCellInfo cellWithText:text text2:text2 actionBlock:NULL];
    return cell;
}

+ (UTCellInfo *)cellWithText:(NSString *)text text2:(NSString *)text2 type:(UTCellType)type
{
    return [UTCellInfo cellWithText:text text2:text2 type:type actionBlock:NULL];
}

+ (UTCellInfo *)cellWithText:(NSString *)text text2:(NSString *)text2 actionBlock:(UTCellActionBlock)block
{
    UTCellInfo *cell = [UTCellInfo cellWithType:block == NULL ? kUTCellTypeText : kUTCellTypeChoose actionBlock:block];
    cell.text = text;
    cell.text2 = text2;
    return cell;
}

+ (UTCellInfo *)cellWithText:(NSString *)text type:(UTCellType)type
{
    UTCellInfo *cell = [UTCellInfo cellWithType:type actionBlock:NULL];
    cell.text = text;
    return cell;
}

+ (UTCellInfo *)cellWithFormat:(NSString *)text, ...
{
    va_list args;
    va_start(args, text);
    NSString *resultText = [[NSString alloc] initWithFormat:text arguments:args];
    va_end(args);
    return [UTCellInfo cellWithText:resultText text2:nil actionBlock:NULL];
}

+ (UTCellInfo *)cellWithText:(NSString *)text
{
    return [UTCellInfo cellWithText:text text2:nil actionBlock:NULL];
}

+ (UTCellInfo *)cellWithText:(NSString *)text type:(UTCellType)type actionBlock:(UTCellActionBlock)block
{
    UTCellInfo *info = [UTCellInfo cellWithType:type actionBlock:block];
    info.text = text;
    return info;
}

+ (UTCellInfo *)cellWithText:(NSString *)text userInfo:(id)userInfo actionBlock:(UTCellActionBlock)block
{
    UTCellInfo *info = [self cellWithText:text actionBlock:block];
    info.userInfo = userInfo;
    return info;
}

+ (UTCellInfo *)cellWithText:(NSString *)text actionBlock:(UTCellActionBlock)block
{
    return [UTCellInfo cellWithText:text text2:nil actionBlock:block];
}

- (void)updateIdentifier
{
    NSString *res = nil;
    switch (self.type)
    {
        case kUTCellTypeText:
            res = @"kUTCellDefault";
            break;
        case kUTCellTypeChoose:
            res = @"kUTCellChoose";
            break;
        default:
            res = @"kUTCellSwitch";
            break;
    }
    self.identifier = [NSString stringWithFormat:@"%@_%llu", res, (u_int64_t)self.style];
}

- (void)setStyle:(UITableViewCellStyle)style
{
    _style = style;
    [self updateIdentifier];
}

- (BOOL)canPerformAction
{
    return self.block != NULL;
}

- (void)performAction
{
    if (NULL != self.block)
    {
        self.block(self);
    }
}

- (void)changeState:(id)sender
{
    if ([sender isKindOfClass:[UISwitch class]])
    {
        UISwitch *sw = sender;
        self.on = [sw isOn];
        [self performAction];
    }
}

- (void)reload
{
    __strong UITableView *tableView = self.tableView;
    if (nil != tableView && nil != self.indexPath)
    {
        [tableView reloadRowsAtIndexPaths:@[self.indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

- (void)setShowActivityIndicator:(BOOL)showActivityIndicator
{
    _showActivityIndicator = showActivityIndicator;
    [self reload];
}

@end

@implementation UTCellInfo(NavigationCells)

+ (UTCellInfo *)imageViewerCellWithText:(NSString *)text navigationController:(UINavigationController *)nc object:(NSObject *)obj keyPath:(NSString *)keyPath
{
    UTCellInfo *cellInfo = [UTCellInfo cellWithType:kUTCellTypeChoose actionBlock:^(UTCellInfo *info) {
        UTTableImageViewController *vc = [UTTableImageViewController new];
        [vc setObject:obj andKeyPath:keyPath];
        [nc pushViewController:vc animated:YES];
    }];
    cellInfo.text = text;
    return cellInfo;
}

+ (UTCellInfo *)textViewerCellWithText:(NSString *)text text2:(NSString *)text2 navigationController:(UINavigationController *)nc
{
    UTCellInfo *cellInfo = [UTCellInfo cellWithType:kUTCellTypeChoose actionBlock:^(UTCellInfo *info) {
        UTTableTextViewController *vc = [UTTableTextViewController new];
        [vc setText:text2];
        [nc pushViewController:vc animated:YES];
    }];
    cellInfo.text = text;
    cellInfo.text2 = text2;
    return cellInfo;
}

@end
