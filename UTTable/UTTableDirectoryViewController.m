//
//  UTTableDirectoryViewController.m
//  UTTable
//
//  Created by Maksym Kareta on 5/12/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

#import "UTTableDirectoryViewController.h"
#import "UTTableFileViewController.h"
#import "UTCellInfo.h"

@interface UTTableDirectoryViewController ()

@end

@implementation UTTableDirectoryViewController

- (NSArray *)buildItems
{
    UTCellActionBlock block = ^(UTCellInfo *info) {
        if (NULL != self.actionBlock)
        {
            self.actionBlock(info.userInfo);
        }
        else
        {
            UTTableFileViewController *vc = [UTTableFileViewController new];
            vc.filePath = info.userInfo;
            [self.navigationController pushViewController:vc animated:YES];
        }
    };
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *files = [fileManager contentsOfDirectoryAtPath:self.path error:nil];
    NSMutableArray *items = [NSMutableArray new];
    for (NSString *path in files)
    {
        NSString *fullPath = [NSString stringWithFormat:@"%@/%@", self.path, path];
        BOOL directory = NO;
        BOOL exists = [fileManager fileExistsAtPath:fullPath isDirectory:&directory];
        if ([self.extensions containsObject:path.pathExtension] && exists && !directory)
        {
            UTCellInfo *item = [UTCellInfo cellWithText:path type:kUTCellTypeChoose actionBlock:block];
            item.userInfo = fullPath;
            [items addObject:item];
        }
    }
    return items;
}

@end
