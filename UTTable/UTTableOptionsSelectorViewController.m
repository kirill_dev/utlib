//
//  DemoOptionsSelectorTableViewController.m
//  iDroydSDKDemo
//
//  Created by Maksym Kareta on 3/13/14.
//
//

#import "UTTableOptionsSelectorViewController.h"

@interface UTTableOptionsSelectorViewController () <UTOptionsSelectorDelegate>

@property (strong, nonatomic) NSArray *options;
@property (strong, nonatomic) id selectedValue;

@end

@implementation UTTableOptionsSelectorViewController

- (id)initWithOptions:(NSArray *)options
{
    self = [super initWithStyle:UITableViewStylePlain];
    if (nil != self)
    {
        _options = options;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"default"];
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.options.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"default" forIndexPath:indexPath];
    [[cell textLabel] setText:self.options[indexPath.row][@"text"]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    id value = self.options[indexPath.row][@"value"];
    if ([value isKindOfClass:[NSDictionary class]] && nil != [value objectForKey:@"options"])
    {
        self.selectedValue = [value objectForKey:@"value"];
        NSArray* options = [value objectForKey:@"options"];
        UTTableOptionsSelectorViewController *vc = [[UTTableOptionsSelectorViewController alloc] initWithOptions:options];
        vc.delegate = self;
        [self.navigationController pushViewController:vc animated:YES];
    }
    else
    {
        self.selectedValue = value;
        [self didSelectOptions:@[value]];
    }
}

- (void)didSelectOptions:(NSArray *)options
{
    NSMutableArray* finalOptions = [NSMutableArray arrayWithObject:self.selectedValue];
    [finalOptions addObjectsFromArray:options];
    id <UTOptionsSelectorDelegate> delegate = self.delegate;
    [delegate optionsSelector:self didSelectOptions:finalOptions];
}

- (void)optionsSelector:(UTTableOptionsSelectorViewController *)vc didSelectOptions:(NSArray *)options
{
    [self didSelectOptions:options];
}

@end
