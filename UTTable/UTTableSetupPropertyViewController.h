//
//  UTTableSetupPropertyViewController.h
//  UTLib
//
//  Created by Maksym Kareta on 5/12/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

#import "UTTableViewController.h"
#import "UTTableOptionsSelectorViewController.h"

@interface UTTableSetupPropertyViewController : UTTableOptionsSelectorViewController

- (instancetype)initWithObject:(NSObject *)obj keyPath:(NSString *)keyPath values:(NSArray *)values;

@end
