//
//  UITableViewCell+CellCreation.h
//  UTTable
//
//  Created by Maksym Kareta on 5/12/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UTCellInfo;

@interface UITableViewCell(CellCreation)

+ (UITableViewCell *)cellWithInfo:(UTCellInfo *)info;
- (void)updateWithInfo:(UTCellInfo *)info;

@end
