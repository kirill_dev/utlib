//
//  UTTableDirectoryViewController.h
//  UTTable
//
//  Created by Maksym Kareta on 5/12/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

#import "UTTableViewController.h"
#import "GCD+UTFunctions.h"

@interface UTTableDirectoryViewController : UTTableViewController

@property (strong, nonatomic) NSSet *extensions;
@property (strong, nonatomic) NSString *path;

///Action block that will fire when user select file. Obj is NSString path to file. May be NULL.
/// If NULL UTTableFileViewController will be used to show content.
@property (copy, nonatomic) UTBlockWithObject actionBlock;

@end
