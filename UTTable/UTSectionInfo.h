//
//  UTSectionInfo.h
//  UTLib
//
//  Created by Maksym Kareta on 8/16/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

@class UTCellInfo;

@interface UTSectionInfo : NSObject

@property (assign, nonatomic) NSInteger index;
@property (weak, nonatomic) UITableView *tableView;
@property (strong, nonatomic, readonly) NSString *headerTitle;
@property (strong, nonatomic, readonly) NSString *footerTitle;
@property (strong, nonatomic, readonly) NSArray *items;

+ (instancetype)sectionWithItems:(NSArray *)items;
+ (instancetype)sectionWithItems:(NSArray *)items headerTitle:(NSString *)title;
+ (instancetype)sectionWithItems:(NSArray *)items footerTitle:(NSString *)title;
+ (instancetype)sectionWithItems:(NSArray *)items headerTitle:(NSString *)header footerTitle:(NSString *)footer;

@end


@interface UTMutableSectionInfo : UTSectionInfo

@property (strong, nonatomic, readonly) NSMutableArray *items;

- (void)appendWithCellInfo:(UTCellInfo *)cellInfo;

@end

