//
//  UTTableFileViewController.h
//  UTTable
//
//  Created by Maksym Kareta on 5/12/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

#import "UTTableTextViewController.h"

@interface UTTableFileViewController : UTTableTextViewController

@property (strong, nonatomic) NSString *filePath;

@end
