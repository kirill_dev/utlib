//
//  UTVectorTest.m
//  UsefulThings
//
//  Created by Maksym Kareta on 3/17/12.
//  Copyright (c) 2012 Maksym Kareta. All rights reserved.
//

#import "UTVectorTest.h"
#import "UTVector.h"

@implementation UTVectorTest
{
	UTVector _vec1;
	UTVector _vec2;
	UTVector _vec3;
	UTVector _vec4;
	UTVector _vec5;
}

- (void)setUp
{
    [super setUp];
    _vec1 = UTVectorMake(10, 12);
	_vec2 = UTVectorMake(-5, 17);
	_vec3 = UTVectorMake(0, 0);
	_vec4 = UTVectorMake(-3, -3);
	_vec5 = UTVectorMake(3, 3);
	
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testCreation
{
	UTVector x = UTVectorMake(0, -3);
	XCTAssertEqualsWithAccuracy(x.x, 0.0f, 0.000001, @"error while test vector creation");
	XCTAssertEqualsWithAccuracy(x.y, -3.0f, 0.000001, @"error while test vector creation");
}

- (void)testVectorsAverage
{
	UTVector x = UTVectorVectorsAverage(_vec1, _vec2);
	XCTAssertEqualsWithAccuracy(x.x, 2.5f, 0.000001, @"error testVectorsAverage");
	XCTAssertEqualsWithAccuracy(x.y, 14.5f, 0.000001, @"error testVectorsAverage");
	
	x = UTVectorVectorsAverage(_vec3, _vec3);
	
	XCTAssertEqualsWithAccuracy(x.x, 0.0f, 0.000001, @"error testVectorsAverage");
	XCTAssertEqualsWithAccuracy(x.y, 0.0f, 0.000001, @"error testVectorsAverage");
	
	x = UTVectorVectorsAverage(_vec4, _vec5);
	
	XCTAssertEqualsWithAccuracy(x.x, 0.0f, 0.000001, @"error testVectorsAverage");
	XCTAssertEqualsWithAccuracy(x.y, 0.0f, 0.000001, @"error testVectorsAverage");
}

- (void)testVectorsSum
{
	UTVector x = UTVectorSumVectors(_vec1, _vec2);
	XCTAssertEqualsWithAccuracy(x.x, 5.0f, 0.000001, @"error testVectorsSum");
	XCTAssertEqualsWithAccuracy(x.y, 29.0f, 0.000001, @"error testVectorsSum");
	
	x = UTVectorSumVectors(_vec3, _vec3);
	
	XCTAssertEqualsWithAccuracy(x.x, 0.0f, 0.000001, @"error testVectorsSum");
	XCTAssertEqualsWithAccuracy(x.y, 0.0f, 0.000001, @"error testVectorsSum");
	
	x = UTVectorSumVectors(_vec4, _vec5);
	
	XCTAssertEqualsWithAccuracy(x.x, 0.0f, 0.000001, @"error testVectorsSum");
	XCTAssertEqualsWithAccuracy(x.y, 0.0f, 0.000001, @"error testVectorsSum");
}

- (void)testVectorsSubtrac
{
	UTVector x = UTVectorSubtracVectors(_vec1, _vec2);
	XCTAssertEqualsWithAccuracy(x.x, 15.0f, 0.000001, @"error testVectorsSubtrac");
	XCTAssertEqualsWithAccuracy(x.y, -5.0f, 0.000001, @"error testVectorsSubtrac");
	
	x = UTVectorSubtracVectors(_vec3, _vec3);
	
	XCTAssertEqualsWithAccuracy(x.x, 0.0f, 0.000001, @"error testVectorsSubtrac");
	XCTAssertEqualsWithAccuracy(x.y, 0.0f, 0.000001, @"error testVectorsSubtrac");
	
	x = UTVectorSubtracVectors(_vec4, _vec5);
	
	XCTAssertEqualsWithAccuracy(x.x, -6.0f, 0.000001, @"error testVectorsSubtrac");
	XCTAssertEqualsWithAccuracy(x.y, -6.0f, 0.000001, @"error testVectorsSubtrac");
}

- (void)testVectorsScale
{
	UTVector x = UTVectorScale(_vec1, 0.0, 1.0);
	XCTAssertEqualsWithAccuracy(x.x, 0.0f, 0.000001, @"error testVectorsScale");
	XCTAssertEqualsWithAccuracy(x.y, 12.0f, 0.000001, @"error testVectorsScale");
	
	x = UTVectorScale(_vec3, 3.0, 0.0);
	
	XCTAssertEqualsWithAccuracy(x.x, 0.0f, 0.000001, @"error testVectorsScale");
	XCTAssertEqualsWithAccuracy(x.y, 0.0f, 0.000001, @"error testVectorsScale");
	
	x = UTVectorScale(_vec4, -1.0, -1.0);
	
	XCTAssertEqualsWithAccuracy(x.x, 3.0f, 0.000001, @"error testVectorsScale");
	XCTAssertEqualsWithAccuracy(x.y, 3.0f, 0.000001, @"error testVectorsScale");
}

- (void)testVectorsMultiply
{
	UTVector x = UTVectorMultipliedByScalar(_vec1, 1.0);
	XCTAssertEqualsWithAccuracy(x.x, _vec1.x, 0.000001, @"error testVectorsMultiply");
	XCTAssertEqualsWithAccuracy(x.y, _vec1.y, 0.000001, @"error testVectorsMultiply");
	
	x = UTVectorMultipliedByScalar(_vec3, 3.0);
	
	XCTAssertEqualsWithAccuracy(x.x, 0.0f, 0.000001, @"error testVectorsMultiply");
	XCTAssertEqualsWithAccuracy(x.y, 0.0f, 0.000001, @"error testVectorsMultiply");
	
	x = UTVectorMultipliedByScalar(_vec4, -1.0);
	
	XCTAssertEqualsWithAccuracy(x.x, -_vec4.x, 0.000001, @"error testVectorsMultiply");
	XCTAssertEqualsWithAccuracy(x.y, -_vec4.y, 0.000001, @"error testVectorsMultiply");
}

- (void)testVectorsAngle
{
	CGFloat angle = UTVectorAngle(_vec3);
	XCTAssertEqualsWithAccuracy(angle, 0.0f, 0.000001, @"error testVectorsAngle");
	
	angle = UTVectorAngle(_vec5);
	XCTAssertEqualsWithAccuracy(angle, (CGFloat)M_PI_4, 0.000001, @"error testVectorsAngle");
	
	angle = UTVectorAngle(_vec4);
	XCTAssertEqualsWithAccuracy(angle, (CGFloat)(3.0 * M_PI / 4.0), 0.000001, @"error testVectorsAngle");
	
}

#warning add following tests 
/*
  UTVector UTVectorRotate(UTVector vector, CGFloat angle);
  UTVector UTVectorNormalize(UTVector vector);
  CGFloat UTVectorLength(UTVector vector);
  UTVector UTVectorSetLength(UTVector vector, CGFloat length);
  UTVector UTVectorSmoothStep(UTVector vector1, UTVector vector2, CGFloat step);

 */
@end
