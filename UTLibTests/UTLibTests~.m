//
//  UTLibTests.m
//  UTLibTests
//
//  Created by Max Kareta on 9/18/12.
//  Copyright (c) 2012 Max Kareta. All rights reserved.
//

#import "UTLibTests.h"

@implementation UTLibTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    STFail(@"Unit tests are not implemented yet in UTLibTests");
}

@end
