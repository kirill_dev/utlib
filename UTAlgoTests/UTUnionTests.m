//
//  UTUnionTests.m
//  UTLib
//
//  Created by Maksym Kareta on 2/16/14.
//  Copyright (c) 2014 Maksym Kareta. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "UTUnion.h"

@interface UTUnionTests : XCTestCase

@property (assign, nonatomic) UTUnionRef unionRef;

@end

@implementation UTUnionTests

- (void)setUp
{
    [super setUp];
    self.unionRef = UTUnionNew(1024);
}

- (void)tearDown
{
    UTUnionFree(self.unionRef);
    self.unionRef = NULL;
    [super tearDown];
}

- (void)testSimpleUnion
{
    UTUnionConnect(self.unionRef, 0, 1);
    XCTAssertTrue(UTUnionIsConnected(self.unionRef, 0, 1), @"");
}

- (void)testSimpleChainUnion
{
    UTUnionConnect(self.unionRef, 0, 1);
    UTUnionConnect(self.unionRef, 1, 2);
    UTUnionConnect(self.unionRef, 3, 4);
    UTUnionConnect(self.unionRef, 2, 3);
    XCTAssertTrue(UTUnionIsConnected(self.unionRef, 0, 4), @"");
}

- (void)testLongChainUnion
{
    for (int i = 0; i < 1023; i++)
    {
        UTUnionConnect(self.unionRef, i, i + 1);
    }
    XCTAssertTrue(UTUnionIsConnected(self.unionRef, 0, 1023), @"");
}

@end
