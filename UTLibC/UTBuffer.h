//
//  PABuffer.h
//  UTLib
//
//  Created by Maksym Kareta on 10/12/12.
//  Copyright (c) 2012 Maksym Kareta. All rights reserved.
//

#ifndef UTLib_UTBuffer_h
#define UTLib_UTBuffer_h

#include "UTDefinitionsC.h"

typedef struct UTBuffer UTBuffer;

UT_EXTERN UTBuffer * UTBufferCreate(void);
UT_EXTERN void UTBufferFree(UTBuffer *inBuf);
UT_EXTERN size_t UTBufferSize(UTBuffer *inBuf);

UT_EXTERN void UTBufferWrite(UTBuffer *inBuf, void *inMem, size_t size);
UT_EXTERN void UTBufferWriteNoCopy(UTBuffer *inBuf, void *inMem, size_t size, bool freeWhenDone);

UT_EXTERN size_t UTBufferRead(UTBuffer *inBuf, void **outMem, size_t length, bool createNew);
UT_EXTERN size_t UTBufferReadFast(UTBuffer *inBuf, void **outMem);//outMem should be NULL

#endif /* defined(UTLib_UTBuffer_h) */
