//
//  UTDefinitionsC.h
//  UTLib
//
//  Created by Maksym Kareta on 11/24/12.
//  Copyright (c) 2012 Maksym Kareta. All rights reserved.
//

#ifndef UTLib_UTDefinitionsC_h
#define UTLib_UTDefinitionsC_h

#if defined(__cplusplus)
    #define UT_EXTERN_ALWAYS_INLINE extern "C" inline __attribute__((always_inline))
    #define UT_EXTERN_INLINE extern "C" inline
    #define UT_EXTERN extern "C"
#else
    #define UT_EXTERN_ALWAYS_INLINE extern inline __attribute__((always_inline))
    #define UT_EXTERN_INLINE extern inline
    #define UT_EXTERN extern
#endif

#define UT_ALWAYS_INLINE inline __attribute__((always_inline))
#define UT_INLINE inline
#define UT_STATIC_INLINE static inline

#include "stdlib.h"
#include "string.h"
#include "stdbool.h"
#include <assert.h>
#include <sys/types.h>

#endif
