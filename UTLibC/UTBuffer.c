//
//  PABuffer.cpp
//  UTLib
//
//  Created by Maksym Kareta on 10/12/12.
//  Copyright (c) 2012 Maksym Kareta. All rights reserved.
//

#include "UTBuffer.h"

struct UTBufferChunk
{
	char *mem;
    size_t size;
    char *readPointer;
    bool isMemoryOwner;
    struct UTBufferChunk *next;
};
typedef struct UTBufferChunk UTBufferChunk;

struct UTBuffer
{
	UTBufferChunk *first;
    UTBufferChunk *last;
    size_t size;
};

size_t UTBufferSize(UTBuffer *inBuf)
{
    return inBuf->size;
}

void UTBufferFreeChunk(UTBufferChunk* chunk);
void UTBufferPopFrontChunk(UTBuffer* buf);
void UTBufferWriteInternal(UTBuffer* inBuf, void *inMem, size_t inSize, bool shouldCopy, bool freeWhenDone);

void UTBufferFreeChunk(UTBufferChunk* chunk)
{
    if (chunk->isMemoryOwner)
    {
        free(chunk->mem);
    }
	free(chunk);
}

void UTBufferPopFrontChunk(UTBuffer* buf)
{
    UTBufferChunk *chunk = buf->first;
    buf->first = chunk->next;
    UTBufferFreeChunk(chunk);
    if (buf->first == NULL)
    {
        buf->last = NULL;
    }
}

UTBuffer * UTBufferCreate()
{
    UTBuffer *buf = (UTBuffer*)malloc(sizeof(UTBuffer));
    buf->size = 0;
    buf->first = NULL;
    buf->last = NULL;
    return buf;
}

void UTBufferFree(UTBuffer *buffer)
{
	UTBufferChunk *chunk = buffer->first;
    while (chunk != NULL)
    {
        UTBufferChunk *next = chunk->next;
        UTBufferFreeChunk(chunk);
		chunk = next;
    }
    free(buffer);
}

void UTBufferWrite(UTBuffer *inBuf, void *inMem, size_t inSize)
{
    UTBufferWriteInternal(inBuf, inMem, inSize, true, true);
}

void UTBufferWriteNoCopy(UTBuffer *inBuf, void *inMem, size_t inSize, bool freeWhenDone)
{
    UTBufferWriteInternal(inBuf, inMem, inSize, true, freeWhenDone);
}

void UTBufferWriteInternal(UTBuffer* inBuf, void *inMem, size_t inSize, bool shouldCopy, bool freeWhenDone)
{
    UTBufferChunk* chunk = (UTBufferChunk *)malloc(sizeof(UTBufferChunk));
    chunk->next = NULL;
    chunk->size = inSize;
    chunk->isMemoryOwner = freeWhenDone;
	if (shouldCopy)
    {
        chunk->mem = (char*)malloc(inSize);
        memcpy(chunk->mem, inMem, inSize);
    }
    else
    {
        chunk->mem = (char*)inMem;
    }
    chunk->readPointer = chunk->mem;
    if (inBuf->last == NULL)
    {
        inBuf->first = chunk;
        inBuf->last = chunk;
    }
    else
    {
		inBuf->last->next = chunk;
        inBuf->last = chunk;
    }
    inBuf->size += inSize;
}

size_t UTBufferRead(UTBuffer *inBuf, void **outMemory, size_t length, bool createNew)
{
	char **outMem = (char**)outMemory;
    length = inBuf->size < length ? inBuf->size : length;
	if (length > 0)
    {
		if (createNew)
        {
            assert(outMem != NULL);
            *outMem = (char *)malloc(length);
        }
        size_t lengthLeft = length;
        char *outMemPointer = *outMem;
        while (lengthLeft > 0)
        {
            UTBufferChunk *chunk = inBuf->first;
        	size_t size = chunk->size;
            size_t readSize = lengthLeft < size ? lengthLeft : size;
            memcpy(outMemPointer, chunk->readPointer, readSize);
            chunk->readPointer += readSize;
            chunk->size -= readSize;
            if (chunk->size == 0)
            {
                UTBufferPopFrontChunk(inBuf);
            }
            lengthLeft -= readSize;
            outMemPointer += readSize;
            inBuf->size -= readSize;
        }
    }
	return length;
}

size_t UTBufferReadFast(UTBuffer *inBuf, void **outMemory)
{
    assert(outMemory != NULL);
	char **outMem = (char**)outMemory;
    char *outMemPointer = *outMem;
    size_t resultSize = 0;
    UTBufferChunk *chunk = inBuf->first;
    if (NULL != chunk)
    {
        resultSize = chunk->size;
        if (chunk->isMemoryOwner && chunk->readPointer == chunk->mem)
        {
            *outMem = chunk->mem;
            chunk->isMemoryOwner = false;
        }
        else
        {
            *outMem = (char *)malloc(resultSize);
            memcpy(outMemPointer, chunk->readPointer, resultSize);
        }
        UTBufferPopFrontChunk(inBuf);
        inBuf->size -= resultSize;
    }
    return resultSize;

}
